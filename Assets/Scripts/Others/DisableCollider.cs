﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableCollider : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if(TutorialManager.Instance.IsCompleted) GetComponent<Collider>().enabled = false;
    }

    private void OnCollisionStay(Collision other)
    {
        if(TutorialManager.Instance.IsCompleted) GetComponent<Collider>().enabled = false;
    }

    private void OnCollisionExit(Collision other)
    {
        if(TutorialManager.Instance.IsCompleted) GetComponent<Collider>().enabled = false;
    }
}
