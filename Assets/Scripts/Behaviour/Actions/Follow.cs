using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

namespace G1.IA
{
    public class Follow : Action
    {
        public SharedTransform Target;

        private GroundMovement _GroundMovement;
        private Animator _Animator;
        private Controller _Controlador;
        private Agent _Agent;

        private Vector3 _Direction;
        private GameObject _Player;

        public override void OnStart()
        {
            _Animator = GetComponent<Animator>();
            _GroundMovement = GetComponent<GroundMovement>();
            _Agent = GetComponent<Agent>();
            _Animator.SetFloat("Speed", _GroundMovement.MovementSpeed);
            _Player = GameObject.FindGameObjectWithTag("Player");
        }

        public override TaskStatus OnUpdate()
        {
            if (_Player == null)
            {
                _Player = GameObject.FindGameObjectWithTag("Player");
            }
            Vector3 distance = _Player.transform.position - transform.position;
            float Magnitude = Vector3.SqrMagnitude(distance);
            if ( Magnitude < 3f)
            {
                _GroundMovement.Stop();
                _GroundMovement.MovementSpeed = 0;
                _Animator.SetFloat("Speed", _GroundMovement.MovementSpeed);
                return TaskStatus.Success;
            }
            else
            {
                if (Magnitude > 700f)
                {
                    _GroundMovement.Stop();
                    _GroundMovement.MovementSpeed = 0;
                    _Animator.SetFloat("Speed", _GroundMovement.MovementSpeed);
                    return TaskStatus.Success;
                }
                else
                {
                    _Agent.MovementState.ChangeState(AgentStates.MovementStates.Walking);
                    _Animator.SetFloat("Speed", _GroundMovement.MovementSpeed);
                    transform.LookAt(_Player.transform);
                    transform.position = Vector3.MoveTowards(transform.position, _Player.transform.position,
                        (_GroundMovement.MovementSpeed * Time.deltaTime));
                    return TaskStatus.Running;
                }
            }
        }
    }
}