using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class PosibilidadEsquivar : Conditional
{
   
	public override TaskStatus OnUpdate()
	{
        int posiblidad = Random.Range(1,3);
        if (posiblidad == 1)
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
	}
}