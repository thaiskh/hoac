﻿using System;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks.Unity.UnityGameObject;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class BossBattleManager : MonoBehaviour
{
    public GameObject paredBoss;

    public AudioClip BossBattleSFX;

    private GameObject m_Player;
    private Agent m_Agent;

    private void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");
        m_Agent = m_Player.GetComponent<Agent>();
    }

    private void Update()
    {
        if (m_Player == null)
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
            m_Agent = m_Player.GetComponent<Agent>();
        }
        if (m_Agent.ConditionState.CurrentState == AgentStates.AgentConditions.Dead)
        {
            paredBoss.SetActive(false);
            SoundManager.Instance.StopBackgroundMusic();
        }
    }

    public void PlayMusic()
    {
        SoundManager.Instance.StopBackgroundMusic();
        SoundManager.Instance.PlayBackgroundMusic(BossBattleSFX);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            paredBoss.SetActive(true);
        }
    }
    
}
