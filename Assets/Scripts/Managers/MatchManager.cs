﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class SerializedGame: SerializableData
{
    public string PlayerName;
    
    public string SceneName;

    public float[] Position;

    public int CurrentCoins;

    public LevelSystem LevelSystem;
    
    public SavePoint LastCheckPoint;
}

public struct CoinsEvent
{
    public enum CoinsMethods
    {
        Set,
        Add,
        Remove
    }

    public CoinsMethods CoinsMethod;
    public int Quantity;

    public CoinsEvent(CoinsMethods spiritMethod, int quantity)
    {
        CoinsMethod = spiritMethod;
        Quantity = quantity;
    }
}

public struct XpEvent
{
    public enum XPMethods
    {
        Add,
        Remove
    }

    public XPMethods XPMethod;
    public int Quantity;

    public XpEvent(XPMethods xpMethod, int quantity)
    {
        XPMethod = xpMethod;
        Quantity = quantity;
    }
}

public struct MatchEvent
{
    public enum MatchEventType
    {
        Death, Completed, Loaded, Ready
    }

    public MatchEventType Type;

    public MatchEvent(MatchEventType type)
    {
        Type = type;
    }
}

public struct NewZoneEvent
{
    public Zone Zone;

    public NewZoneEvent(Zone zone)
    {
        Zone = zone;
    }
}

public class GameEvents
{
    public const string SAVE_GAME = "SaveGame";
    public const string RESTORE = "Restore";
}

[RequireComponent(typeof(LevelManager))]
public class MatchManager : PersistentSingleton<MatchManager>, EventListener<CoinsEvent>, EventListener<XpEvent>, EventListener<MatchEvent>, EventListener<GameEvent>, EventListener<QuestEvent>, EventListener<NewZoneEvent>, EventListener<AchievementEvent>, ISerialize<SerializedGame>
{
    [Header("Characters")]
    public GameObject WomanCharacter;
    public GameObject ManCharacter;
    [CharacterID]
    public int DefaultCharacter = 1;

    [Header("Stats")]
    public Stat WinGoldData;
    public Stat WasteGoldData;

    [Header("SFX")] 
    public AudioClip MoneySFX;
    
    [HideInInspector]
    public LevelSystem LevelSystem;
    
    private const int MAX_COINS = 999999;
    
    private Vector3 m_InitialPosition;

    public int CurrentCoins { get; set; }
    public SavePoint LastCheckPoint { get; set; }

    public Character Character { get; set; }
    
    public bool MatchLoaded { get; set; }

    public bool AreAllDead {
        get {
            if(Character.ConditionState.CurrentState != AgentStates.AgentConditions.Dead) 
            { 
                return false; 
            } 
            return true;
        }
    }

    void OnEnable()
    {
        this.EventStartListening<CoinsEvent>();
        this.EventStartListening<XpEvent>();
        this.EventStartListening<MatchEvent>();
        this.EventStartListening<QuestEvent>();
        this.EventStartListening<NewZoneEvent>();
        this.EventStartListening<AchievementEvent>();
    }

    void OnDisable()
    {
        this.EventStopListening<CoinsEvent>();
        this.EventStopListening<XpEvent>();
        this.EventStopListening<MatchEvent>();
        this.EventStopListening<QuestEvent>();
        this.EventStopListening<NewZoneEvent>();
        this.EventStopListening<AchievementEvent>();
    }
    
    protected override void Awake()
    {
        base.Awake();

        if (this == _instance)
        {
            LevelSystem.Initialize();
        }
        
    }
    void Start()
    {
        m_InitialPosition = LevelManager.CurrentLevelManager.DebugSpawn.position;
        LoadAdditionalScenesIfRequired();
        StartCoroutine(IsLoadingFinished());
    }

    private void LoadAdditionalScenesIfRequired()
    {
        List<string> loadingScenes = new List<string>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            loadingScenes.Add(SceneManager.GetSceneAt(i).name);
        }

        foreach (var scene in GameManager.Instance.AdditionalGameScenes)
        {
            if (!loadingScenes.Contains(scene)) SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
        }
    }
    
    private IEnumerator IsLoadingFinished()
    {
        yield return new WaitUntil(() =>
        {
            foreach (var scene in GameManager.Instance.AdditionalGameScenes)
            {
                if (!SceneManager.GetSceneByName(scene).isLoaded) return false;
            }

            return true;

        });

        EventManager.TriggerEvent(new MatchEvent(MatchEvent.MatchEventType.Loaded));
    }

    public void InstantiateMainCharacter()
    {
        Character character;
        if (GameManager.Instance.SelectedCharacter == 0)
        {
            GameManager.Instance.SelectedCharacter = DefaultCharacter;
        }
        switch (GameManager.Instance.SelectedCharacter)
        {
            case 1:
                character = Instantiate(WomanCharacter, m_InitialPosition, Quaternion.identity).GetComponent<Character>();
                character.name = WomanCharacter.name;
                break;
            case 2:
                character = Instantiate(ManCharacter, m_InitialPosition, Quaternion.identity).GetComponent<Character>();
                character.name = ManCharacter.name;
                break;
            default:
                character = Instantiate(WomanCharacter, m_InitialPosition, Quaternion.identity).GetComponent<Character>();
                character.name = WomanCharacter.name;
                break;
        }
        character.GetComponent<PlayerInput>().SetController(0);
        Character = character;
        SceneManager.MoveGameObjectToScene(character.gameObject, SceneManager.GetSceneByName("GameCommon"));
        EventManager.TriggerEvent(new MatchEvent(MatchEvent.MatchEventType.Ready));
    }

    public IEnumerator RaiseFromDeath()
    {
        SavePoint checkPoint;
        if(LastCheckPoint == null)
        {
            checkPoint = new SavePoint(SceneManager.GetActiveScene().name, LevelManager.LevelManagerInActiveScene.DebugSpawn.position);
        } 
        else
        {
            checkPoint = LastCheckPoint;
        }

        Character.gameObject.SetActive(false);
        
        EventManager.TriggerEvent(new CoinsEvent(CoinsEvent.CoinsMethods.Remove, Instance.CurrentCoins / 2));

        yield return LoadingSceneManager.Instance.Transition(checkPoint);
        
        Character.gameObject.SetActive(true);
        Vector3 position = new Vector3(checkPoint.CheckPointPosition[0],checkPoint.CheckPointPosition[1],checkPoint.CheckPointPosition[2]);
        Character.Revive(position, 1f);
    }

    public void OnEvent(CoinsEvent eventType)
    {
        if (eventType.Quantity < 0) return;

        switch (eventType.CoinsMethod)
        {
            case CoinsEvent.CoinsMethods.Add:
                AddCoins(eventType.Quantity);
                break;
            case CoinsEvent.CoinsMethods.Remove:
                RemoveCoins(eventType.Quantity);
                break;
            case CoinsEvent.CoinsMethods.Set:
                SetCoins(eventType.Quantity);
                break;
        }

        if (CurrentCoins > MAX_COINS) CurrentCoins = MAX_COINS;
        else if (CurrentCoins < 0) CurrentCoins = 0;

    }

    public void OnEvent(XpEvent eventType)
    {
        switch (eventType.XPMethod)
        {
            case XpEvent.XPMethods.Add:
                AddXp(eventType.Quantity);
                break;
            case XpEvent.XPMethods.Remove:
                break;
        }
    }

   public void AddCoins(int coins)
    {
        CurrentCoins += coins;
        EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Stat, WinGoldData.Id, coins));
        UIHUD.Instance.UpdateCoins(coins);
        SoundManager.Instance.PlaySound(MoneySFX, Character.transform.position);
    }

    public void RemoveCoins(int coins)
    {
        CurrentCoins -= coins;
        EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Stat, WasteGoldData.Id, coins));
        UIHUD.Instance.UpdateCoins(-coins);
    }

    public void SetCoins(int coins)
    {
        CurrentCoins = coins;
        UIHUD.Instance.UpdateCoins(coins);
    }

    public void AddXp(int xp)
    {
        LevelSystem.AddXp(xp);
        UIHUD.Instance.Indicator.UpdateXPBar();
    }

    public void NewZone(Zone zone)
    {
        if (zone != null)
        {
            GUIManager.Instance.ShowZoneName(zone);
            if(LevelManager.CurrentLevelManager.LevelZone != null) EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Explore,LevelManager.CurrentLevelManager.LevelZone.Id));
        }
    }

    public void NewAchievement(Achievement achievement)
    {
        if (achievement != null)
        {
            GUIManager.Instance.ShowNewAchievement();
        }
    }

    public void ExitMatch()
    {
        LoadingSceneManager.Instance.LoadNewScene(GameManager.Instance.ExitMatchScene);
        Inventory.Instance.Clear();
        Destroy(gameObject);
    }

    public void SaveGameState()
    {              
        SerializedGame serializedGame = Serialize();
        SaveLoadManager.SaveGame(serializedGame, GameManager.SAVE_FILE_NAME, GameManager.SAVE_FOLDER_NAME);
    }

    public void OnLoadGame(SerializedGame serializedGame)
    {
        Deserialize(serializedGame);
    }

    public void OnEvent(GameEvent eventType)
    {
        switch (eventType.EventName)
        {
            case GameEvents.SAVE_GAME:
                LastCheckPoint = new SavePoint(SceneManager.GetActiveScene().name, Character.transform.position);
                SaveGameState();
                break;
            case GameEvents.RESTORE:
                Character.Restore();
                break;
        }
    }

    public void OnEvent(MatchEvent eventType)
    {
        switch (eventType.Type)
        {
            case MatchEvent.MatchEventType.Death:
                StartCoroutine(RaiseFromDeath());
                break;
            case MatchEvent.MatchEventType.Loaded:
                InstantiateMainCharacter();
                break;
            case MatchEvent.MatchEventType.Ready:
                MatchLoaded = true;
                break;
        }
    }

    public void OnEvent(QuestEvent eventType)
    {
        UIHUD.Instance.DisplayQuests();
        if (eventType.Type == QuestEvent.EventType.Finished)
        {
            if (eventType.Quest != null)
            {
                AddCoins(eventType.Quest.QuestReward.MoneyReward);
                AddXp(eventType.Quest.QuestReward.ExperienceReward);
                foreach (var item in eventType.Quest.QuestReward.ItemRewards)
                {
                    Inventory.Instance.AddItem(ItemDB.Instance.GetById(item.Item),item.ItemNumber);
                }
            }
        }
    }

    public void OnEvent(NewZoneEvent eventType)
    {
        NewZone(eventType.Zone);
    }

    public void OnEvent(AchievementEvent eventType)
    {
        if(eventType.IsAchieved) NewAchievement(eventType.Achievement);
    }

    public SerializedGame Serialize()
    {
        SerializedGame serializedGame = new SerializedGame
        {
            PlayerName = GameManager.Instance.PlayerName,
            
            SceneName = SceneManager.GetActiveScene().name,
            Position = new []{Character.transform.position.x, Character.transform.position.y, Character.transform.position.z},

            CurrentCoins = CurrentCoins,

            LevelSystem = LevelSystem,
            
            LastCheckPoint = LastCheckPoint,

        };

        return serializedGame;
    }

    public void Deserialize(SerializedGame serializedGame)
    {
        GameManager.Instance.PlayerName = serializedGame.PlayerName;
        m_InitialPosition.x = serializedGame.Position[0];
        m_InitialPosition.y = serializedGame.Position[1];
        m_InitialPosition.z = serializedGame.Position[2];

        CurrentCoins = serializedGame.CurrentCoins;

        LevelSystem = serializedGame.LevelSystem;

        LastCheckPoint = serializedGame.LastCheckPoint;
    }
}

public interface ISerialize<SerializableData>
{
    SerializableData Serialize();

    void Deserialize(SerializableData data);
}

[Serializable]
public class SerializableData
{

}
