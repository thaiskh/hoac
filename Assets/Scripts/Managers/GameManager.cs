﻿using System.Collections;
using System.Collections.Generic;
using PixelCrushers.SceneStreamer;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : PersistentSingleton<GameManager>
{
    public Settings DefaultSettings;

    [Header("Scenes")] [Scene] public string FirstGameScene;
    [Scene] public string CharacterSelectionScene;
    [Scene] public string ExitMatchScene;
    [Scene] public string[] AdditionalGameScenes;

    public bool Paused { get; set; }

    public string PlayerName { get; set; }
    public int SelectedCharacter { get; set; }

    public const string SAVE_FOLDER_NAME = "G1/";
    public const string SAVE_FILE_NAME = "Save";
    public const string SAVE_SET_FILE_NAME = "Settings";

    protected override void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        LoadSettings();
    }


    void Update()
    {
    }

    public static ControllerType GetControllerType()
    {
        Player p = ReInput.players.GetPlayer(0);
        return p.controllers.GetLastActiveController().type;
    }

    public void NewGame(int characterID)
    {
        SelectedCharacter = characterID;
        //LoadingSceneManager.Instance.LoadNewGameScene(FirstGameScene);
        StartCoroutine(LoadScene());
    }

    //Provisional
    public IEnumerator LoadScene()
    {
        yield return SceneManager.LoadSceneAsync("UILoading", LoadSceneMode.Single);
        GUILoading.Instance.Init();
        SceneStreamer.SetCurrentScene(FirstGameScene);
        yield return new WaitUntil(() => SceneStreamer.IsSceneLoaded(FirstGameScene));
        GUILoading.Instance.LoadingReady();

        while (true)
        {
            if (ReInput.controllers.GetAnyButtonDown())
            {
                GUILoading.Instance.Submit();
                break;
            }

            yield return null;
        }

        yield return SceneManager.UnloadSceneAsync("UILoading");
        //SceneStreamer.UnloadScene(CharacterSelectionScene);
    }

    public void LoadGame()
    {
        try
        {
            SerializedGame serializedGame =
                (SerializedGame) SaveLoadManager.LoadGame(SAVE_FILE_NAME, SAVE_FOLDER_NAME);
            LoadingSceneManager.Instance.LoadSavedScene(serializedGame);
        }
        catch (SavedGameNotFoundException e)
        {
            Debug.LogError(e);
        }
    }

    public void Pause()
    {
        if (Time.timeScale > 0f)
        {
            Instance.Paused = true;
            Time.timeScale = 0f;
        }
    }

    public void UnPause()
    {
        Instance.Paused = false;
        if (Time.timeScale == 0f)
        {
            Instance.Paused = false;
            Time.timeScale = 1f;
        }
    }

    public void LoadDefaultSettings()
    {
        SoundManager.Instance.SetMusicVolume(DefaultSettings.MusicDefaultValue);
        SoundManager.Instance.SetSFXVolume(DefaultSettings.SfxDefaultValue);
        LocalizationManager.Instance.LoadLanguage(DefaultSettings.DefaultLanguage);
    }

    public void LoadSavedSettings(SerializedSettings serializedSettings)
    {
        SoundManager.Instance.SetMusicVolume(serializedSettings.MusicValue);
        SoundManager.Instance.SetSFXVolume(serializedSettings.SfxValue);
        LocalizationManager.Instance.LoadLanguage(serializedSettings.Language);
    }

    public void SaveSettings()
    {
        SerializedSettings serializedSettings = new SerializedSettings
        {
            MusicValue = SoundManager.Instance.MusicVolume,
            SfxValue = SoundManager.Instance.SfxVolume,
            Language = LocalizationManager.Instance.CurrentLanguage.Locale
        };

        SaveLoadManager.SaveGame(serializedSettings, SAVE_SET_FILE_NAME, SAVE_FOLDER_NAME);
    }

    public void LoadSettings()
    {
        try
        {
            SerializedSettings serializedSettings =
                (SerializedSettings) SaveLoadManager.LoadGame(SAVE_SET_FILE_NAME, SAVE_FOLDER_NAME);
            if (serializedSettings == null)
            {
                LoadDefaultSettings();
            }
            else
            {
                LoadSavedSettings(serializedSettings);
            }
        }
        catch (SavedGameNotFoundException e)
        {
            LoadDefaultSettings();
        }
    }
}

