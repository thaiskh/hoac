﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired.Utils.Libraries.TinyJson;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ShopEvent : UnityEvent<Character>
{
    
}

[RequireComponent(typeof(Interactable))]
public class NPCShop : MonoBehaviour
{
    public Shop Shop;
    public DialogueGraph Dialogue;

    [Header("ShopEvent")] 
    public ShopEvent OnShopOpen;
    public ShopEvent OnShopClose;
    
    [Header("Shop text")]
    public string MessageText;
    
    public Shop CurrentShop { get; set; }

    private Interactable m_Interactable;

    private void Awake()
    {
        CurrentShop = Instantiate(Shop);

        m_Interactable = GetComponent<Interactable>();
    }

    public void OpenShop(Character character)
    {
        if (m_Interactable.NumberOfActivations == 1)
        {
            GUIManager.Instance.OpenDialogue(Dialogue, character.transform, transform, m_Interactable.NumberOfActivations);
        }
        else
        {
            GUIManager.Instance.OpenMenuShopScreen(this, character.transform);
        }
        
        OnShopOpen.Invoke(character);
    }

    public void CloseShop(Transform opener)
    {
        Character character = opener.GetComponent<Character>();
        OnShopClose.Invoke(character);
    }
}
