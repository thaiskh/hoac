﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class AssetBundleCreator : MonoBehaviour
{
    
    private static void CreateBundle(string path, BuildTarget target)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.ChunkBasedCompression, target);
    }
    
    [MenuItem("Assets/Build Asset Bundle")]
    public static void ExportBundle()
    {
#if UNITY_STANDALONE_WIN
        CreateBundle(Application.streamingAssetsPath + "/AssetBundles/Windows/", BuildTarget.StandaloneWindows64);
#elif UNITY_STANDALONE_OSX
        CreateBundle(Application.streamingAssetsPath + "/AssetBundles/OSX/", BuildTarget.StandaloneOSX);
#elif UNITY_STANDALONE_LINUX
        CreateBundle(Application.streamingAssetsPath + "/AssetBundles/Linux/", BuildTarget.StandaloneOSX);
#endif
    }
}
#endif
