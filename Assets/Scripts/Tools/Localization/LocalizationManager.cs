﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LocalizationManager : PersistentSingleton<LocalizationManager>
{
    private Dictionary<string, Dictionary<string, string>> m_LocalizedText;
    private const string MISSING = "Localized text not found";

    public Language CurrentLanguage { get; set; }

    public Language[] AvailableLang;
    
    public string this[string type, string key]
    {
        get
        {
            if (m_LocalizedText.TryGetValue(type, out Dictionary<string, string> translations))
            {
                if (translations.TryGetValue(key, out string value))
                {
                    return value;
                }
                else
                {
                    return MISSING;
                }
            }
            else
            {
                return MISSING;
            }
        }
    }
    
    public void LoadLocalizedText()
    {
        if (CurrentLanguage == null)
        {
            Debug.Log("Current language is null");
            return;
        }
        
        string filePath = Path.Combine(Application.streamingAssetsPath + "/Localization/", CurrentLanguage.Locale + ".json");
        if (File.Exists(filePath))
        {
            m_LocalizedText = new Dictionary<string, Dictionary<string, string>>();
            string dataAsJson = File.ReadAllText(filePath);
            LocalizedData loadedData = JsonUtility.FromJson<LocalizedData>(dataAsJson);
            for (int i = 0; i < loadedData.Items.Length; i++)
            {
                Dictionary<string, string> translations = new Dictionary<string, string>();
                for (int j = 0; j < loadedData.Items[i].Translations.Length; j++)
                {
                    translations.Add(loadedData.Items[i].Translations[j].Key, loadedData.Items[i].Translations[j].Value);
                }

                m_LocalizedText.Add(loadedData.Items[i].Type, translations);
            }
        }
    }
    
    public void LoadSystemLanguage()
    {
        Language newLanguage;
        switch (Application.systemLanguage)
        {
            case SystemLanguage.Spanish:
                newLanguage = GetLanguageByLocale("es_ES");
                break;
            case SystemLanguage.English:
                newLanguage = GetLanguageByLocale("en_EN");
                break;
            case SystemLanguage.Catalan:
                newLanguage = GetLanguageByLocale("ca_ES");
                break;
            default:
                newLanguage = CurrentLanguage;
                break;
        }

        if (CurrentLanguage != newLanguage)
        {
            CurrentLanguage = newLanguage;
            LoadLocalizedText();
        }
    }
    
    public void LoadLanguage(string locale)
    {
        try
        {
            Language newLanguage = GetLanguageByLocale(locale);
            if(CurrentLanguage != newLanguage)
            {
                CurrentLanguage = newLanguage;
                LoadLocalizedText();
            }
        }
        catch (LanguageNotFoundException)
        {
            Debug.Log("Error getting locale language, keeping current language");
        }
    }
    
    public void LoadLanguage(Language language)
    {
        if(CurrentLanguage != language)
        {
            CurrentLanguage = language;
            LoadLocalizedText();
        }
    }
    
    public Language GetLanguageByLocale(string locale)
    {
        foreach (var l in AvailableLang)
        {
            if (l.Locale == locale) return l;
        }

        throw new LanguageNotFoundException(locale + " not found");
    }
}

public class LanguageNotFoundException : Exception
{
    public LanguageNotFoundException(string message) : base(message)
    {

    }
}

