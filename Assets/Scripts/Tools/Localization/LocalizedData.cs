﻿using System;

[Serializable]
public class LocalizedData
{
    public LocalizedType[] Items;
}

[Serializable]
public class LocalizedType
{
    public string Type;
    public LocalizedItem[] Translations;
}

[Serializable]
public class LocalizedItem
{
    public string Key;
    public string Value;
}
