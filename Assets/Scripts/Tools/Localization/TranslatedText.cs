﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TranslatedText : MonoBehaviour
{
    public enum Letters
    {
        AllLower,
        AllUpper,
        FirstUpper,
        Default
    }

    private TextMeshProUGUI TextPro;
    private Text Text;

    public string PhraseType;
    public string PhraseKey;
    public Letters LetterType;

    public bool SetTextOnStart = true;
    
    void Awake()
    {
        TextPro = GetComponent<TextMeshProUGUI>();
        Text = GetComponent<Text>();
    }
    
    void Start()
    {
        if (SetTextOnStart)
            SetText();
    }
        
    public void SetText()
    {
        if (TextPro != null)
        {
            switch (LetterType)
            {
                case Letters.AllLower:
                    TextPro.text = LocalizationManager.Instance[PhraseType, PhraseKey].ToLower();
                    break;
                case Letters.AllUpper:
                    TextPro.text = LocalizationManager.Instance[PhraseType, PhraseKey].ToUpper();
                    break;
                case Letters.FirstUpper:
                    TextPro.text = FirstCharToUpper(LocalizationManager.Instance[PhraseType, PhraseKey]);
                    break;
                case Letters.Default:
                    TextPro.text = LocalizationManager.Instance[PhraseType, PhraseKey];
                    break;
            }
        }

        if (Text != null)
        {
            switch (LetterType)
            {
                case Letters.AllLower:
                    Text.text = LocalizationManager.Instance[PhraseType, PhraseKey].ToLower();
                    break;
                case Letters.AllUpper:
                    Text.text = LocalizationManager.Instance[PhraseType, PhraseKey].ToUpper();
                    break;
                case Letters.FirstUpper:
                    Text.text = FirstCharToUpper(LocalizationManager.Instance[PhraseType, PhraseKey]);
                    break;
                case Letters.Default:
                    Text.text = LocalizationManager.Instance[PhraseType, PhraseKey];
                    break;
            }
        }
    }
    
    public string FirstCharToUpper(string input)
    {
        switch (input)
        {
            case null: throw new ArgumentNullException(nameof(input));
            case "": throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input));
            default: return input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
    
}
