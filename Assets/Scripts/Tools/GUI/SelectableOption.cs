﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectableOption : Selectable
{
    public UnityEvent Next;
    public UnityEvent Previous;
    
    public override void OnMove(AxisEventData eventData)
    {
        switch (eventData.moveDir)
        {
            case MoveDirection.Left:
                Previous.Invoke();
                break;
            case MoveDirection.Right:
                Next.Invoke();
                break;
            default:
                base.OnMove(eventData);
                break;
        }
    }
}
