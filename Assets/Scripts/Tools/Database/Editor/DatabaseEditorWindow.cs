﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DatabaseEditorWindow : EditorWindow
{
    enum DBType
    {
        Characters, Enemies, Items, NPCs, Quests, Zones, Achievements, Stories
    }
    
    private DBType DatabaseType = DBType.Characters;
    
    [MenuItem("Window/G1/Database Window")]
    static void ShowWindow()
    {
        DatabaseEditorWindow window = (DatabaseEditorWindow)EditorWindow.GetWindow(typeof(DatabaseEditorWindow), false, "Database");
        window.Show();
    }
    
    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();

        DrawDBList();

        switch (DatabaseType)
        {
            case DBType.Characters:
                CharacterDBEditorWindow.DrawWindow();
                break;
            case DBType.Enemies:
                EnemyDBEditorWindow.DrawWindow();
                break;
            case DBType.NPCs:
                NpcDBEditorWindow.DrawWindow();
                break;
            case DBType.Quests:
                QuestDBEditorWindow.DrawWindow();
                break;
            case DBType.Items:
                ItemDBEditorWindow.DrawWindow();
                break;
            case DBType.Zones:
                ZoneDBEditorWindow.DrawWindow();
                break;
            case DBType.Achievements:
                AchievementDBEditorWindow.DrawWindow();
                break;
        }

        EditorGUILayout.EndHorizontal();
    }

    void DrawDBList()
    {
        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();
        
        if (GUILayout.Button("Characters"))
        {
            DatabaseType = DBType.Characters;
        }
        if (GUILayout.Button("Enemies"))
        {
            DatabaseType = DBType.Enemies;
        }
        if (GUILayout.Button("NPCs"))
        {
            DatabaseType = DBType.NPCs;
        }
        if (GUILayout.Button("Quests"))
        {
            DatabaseType = DBType.Quests;
        }
        if (GUILayout.Button("Items"))
        {
            DatabaseType = DBType.Items;
        }
        if (GUILayout.Button("Zones"))
        {
            DatabaseType = DBType.Zones;
        }
        if (GUILayout.Button("Achievements"))
        {
            DatabaseType = DBType.Achievements;
        }
        if (GUILayout.Button("Stories"))
        {
            DatabaseType = DBType.Stories;
        }

        EditorGUILayout.EndVertical();
    }
}

