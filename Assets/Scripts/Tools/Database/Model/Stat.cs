﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Stat", menuName = "G1/Diary/Achievement/Stat")]
public class Stat : ScriptableObject, IDatabaseAsset
{
    public int ID;
    public string StatName;

    public int Id
    {
        get { return ID;}
        set { ID = value; }
    }
    
    public string Name
    {
        get { return StatName;}
        set { StatName = value; }
    }
}
