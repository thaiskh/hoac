﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Interaction", menuName = "G1/Diary/Achievement/Interaction")]
public class Interaction : ScriptableObject, IDatabaseAsset
{
    public int ID;
    public string InteractionName;

    public int Id
    {
        get { return ID;}
        set { ID = value; }
    }
    
    public string Name
    {
        get { return InteractionName;}
        set { InteractionName = value; }
    }
}
