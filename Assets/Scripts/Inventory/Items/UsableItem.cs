﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UsableItem : Item
{
    public abstract bool Usable(Transform target);
    public abstract bool Use(Transform target);
}
