﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CriticalChancePotion", menuName = "G1/Item/Consumable/Critical Chance Potion")]
public class CriticalChancePotion : ConsumableItem, IBuy, ISell
{
    public int BuyValue;
    public int SellValue;

    [Header("Effect")]
    [Range(0F, 1F)]
    public float Increase;
    public float Duration;

    public int BuyPrice => BuyValue;
    public int SellPrice => SellValue;
    
    public override bool Usable(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();

        return (effects != null);
    }
    
    public override bool Use(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();

        effects.AddEffect(new CriticalChanceBuff(Duration, false, Increase));

        return true;
    }
    
    public override string GetDescription()
    {
        string message = base.GetDescription();

        message = message.Replace("%replace%", (Increase * 100).ToString());
        message = message.Replace("%duration%", Duration.ToString());

        return message;
    }
}
