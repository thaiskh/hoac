﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AttackPotion", menuName = "G1/Item/Consumable/Attack Potion")]
public class AttackPotion : ConsumableItem, IBuy, ISell
{
    public int BuyValue;
    public int SellValue;

    [Header("Effect")]
    [Range(0F, 1F)]
    public float Increase;
    public float Duration;

    public int BuyPrice => BuyValue;
    public int SellPrice => SellValue;
    
    public override bool Usable(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();
        Agent agent = target.GetComponent<Agent>();

        return (effects != null && agent != null);
    }
    
    public override bool Use(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();
        Agent agent = target.GetComponent<Agent>();

        int value = (int)(agent.CurrentStats.Attack * Increase);
        effects.AddEffect(new AttackBuff(Duration, false, value));

        return true;
    }
    
    public override string GetDescription()
    {
        string message = base.GetDescription();

        message = message.Replace("%replace%", (Increase * 100).ToString());
        message = message.Replace("%duration%", Duration.ToString());

        return message;
    }
}
