﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class QuestEditorWindow : EditorWindow
{
    private int m_SelectedType = 0;

    private int m_KillQuantity = 1;
    private int m_KillId = 0;
    private int m_ItemQuantity = 1;
    private int m_ItemId = 0;
    private int m_NPCId = 0;
    private int m_ZoneID = 0;

    private Quest m_Scriptable;

    private string[] m_EnemyOptions;
    private string[] m_ZoneOptions;
    private string[] m_NpcOptions;

    public void OpenWindow(Quest quest)
    {
        m_Scriptable = quest;

        m_EnemyOptions = EnemyDB.Instance.GetIds().ConvertAll<string>(x => x.ToString()).ToArray();
        m_ZoneOptions = ZoneDB.Instance.GetIds().ConvertAll<string>(x => x.ToString()).ToArray();
        m_NpcOptions = NpcDB.Instance.GetIds().ConvertAll<string>(x => x.ToString()).ToArray();

        QuestEditorWindow window = (QuestEditorWindow) GetWindow(typeof(QuestEditorWindow), false, "Quest Goal");
        window.minSize = new Vector2(400, 400);
        window.Show();
    }

    void OnGUI()
    {
        if (m_Scriptable == null)
        {
            return;
        }

        float width = position.width - 5;
        float height = 30;

        string[] actionLabels = new string[] {"Kill", "NPC", "Zone", "Tutorial"};

        GUILayout.Label("Choose goal type", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        m_SelectedType = GUILayout.SelectionGrid(m_SelectedType, actionLabels, 4, GUILayout.Width(width),
            GUILayout.Height(height));
        GUILayout.EndHorizontal();

        switch (m_SelectedType)
        {
            case 0:
                GUIKill();
                break;
            case 1:
                GUINPC();
                break;
            case 2:
                GUIZone();
                break;
            case 3:
                GUITutorial();
                break;
        }

        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add Goal", GUILayout.Height(30)))
        {
            SubmitGoal();
        }

        GUILayout.EndHorizontal();
    }

    private void SubmitGoal()
    {
        switch (m_SelectedType)
        {
            case 0:
                EnemySO assetEnemy = EnemyDB.Instance.GetById(m_KillId);
                if (assetEnemy != null)
                {
                    KillGoal goal = new KillGoal(assetEnemy.Id, assetEnemy.Name, m_KillQuantity);
                    m_Scriptable.EnemyAmount.Add(goal);
                }

                break;
            case 1:
                break;
            case 2:
                throw new NotImplementedException();
        }

        Close();
        EditorUtility.SetDirty(m_Scriptable);
        AssetDatabase.Refresh();
    }

    private void GUIKill()
    {
        GUILayout.Space(10);

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Enemy Required", EditorStyles.boldLabel);

        GUI.skin.label.alignment = TextAnchor.MiddleRight;

        GUILayout.BeginVertical(GUILayout.Width(position.width - 20));
        EditorGUIUtility.labelWidth = 50;
        m_KillId = EditorGUILayout.Popup("ID", m_KillId, m_EnemyOptions);
        m_KillQuantity = EditorGUILayout.IntSlider("Quantity", m_KillQuantity, 1, 100);
        GUILayout.EndVertical();

        GUILayout.EndVertical();
    }

    private void GUIZone()
    {
        GUILayout.Space(10);

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Zone Required", EditorStyles.boldLabel);

        GUI.skin.label.alignment = TextAnchor.MiddleRight;

        GUILayout.BeginVertical(GUILayout.Width(position.width - 20));
        EditorGUIUtility.labelWidth = 50;
        m_ItemId = EditorGUILayout.Popup("ID", m_ZoneID, m_ZoneOptions);
        m_ItemQuantity = EditorGUILayout.IntSlider("Quantity", m_ItemQuantity, 1, 100);
        GUILayout.EndVertical();

        GUILayout.EndVertical();
    }

    private void GUINPC()
    {
        GUILayout.Space(10);

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("NPC Required", EditorStyles.boldLabel);

        GUI.skin.label.alignment = TextAnchor.MiddleRight;

        GUILayout.BeginVertical(GUILayout.Width(position.width - 20));
        EditorGUIUtility.labelWidth = 50;
        m_NPCId = EditorGUILayout.Popup("ID", m_NPCId, m_NpcOptions);
        GUILayout.EndVertical();

        GUILayout.EndVertical();
    }

    private void GUITutorial()
    {
        GUILayout.Space(10);

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Action Required", EditorStyles.boldLabel);

        GUI.skin.label.alignment = TextAnchor.MiddleRight;

        GUILayout.BeginVertical(GUILayout.Width(position.width - 20));
        EditorGUIUtility.labelWidth = 50;
        m_NPCId = EditorGUILayout.Popup("ID", m_NPCId, m_NpcOptions);
        GUILayout.EndVertical();

        GUILayout.EndVertical();
    }
}
