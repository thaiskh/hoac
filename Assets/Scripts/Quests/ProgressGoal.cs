﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ProgressGoal
{
    public int ID { get; set; }
    public string Name { get; set; }
    public bool Finished { get; set; }

    public ProgressGoal(int id, string name)
    {
        ID = id;
        Name = name;
    }
}

[Serializable]
public class TutorialProgressGoal : ProgressGoal
{
    public TutorialProgressGoal(int id, string name) : base(id, name)
    {

    }

    public TutorialProgressGoal(TutorialGoal goal) : base(goal.ID, goal.Name)
    {

    }
}

[Serializable]
public class KillProgressGoal : ProgressGoal
{
    public int Required;
    public int Current;

    public KillProgressGoal(int id, string name, int amount) : base(id, name)
    {
        Current = 0;
        Required = amount;
    }

    public KillProgressGoal(KillGoal goal) : base(goal.ID, goal.Name)
    {
        Current = 0;
        Required = goal.RequiredAmount;
    }
}

[Serializable]
public class ActivateNPCProgressGoal : ProgressGoal
{
    public ActivateNPCProgressGoal(int id, string name) : base(id, name)
    {

    }

    public ActivateNPCProgressGoal(ActivateNPCGoal goal) : base(goal.ID, goal.Name)
    {

    }
}

[Serializable]
public class ActivateZoneProgressGoal : ProgressGoal
{
    public ActivateZoneProgressGoal(int id, string name) : base(id, name)
    {

    }

    public ActivateZoneProgressGoal(ActivateZoneGoal goal) : base(goal.ID, goal.Name)
    {

    }
}
