﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface QuestFilter
{
    List<QuestStatus> Filter(List<QuestStatus> toFilter);
}

public class CompletedQuestFilter : QuestFilter
{
    public CompletedQuestFilter()
    {

    }

    public List<QuestStatus> Filter(List<QuestStatus> toFilter)
    {
        return toFilter.Where(x => x.Progression.CurrentState == QuestProgression.State.Completed).ToList();
    }
}

public class AcceptedQuestFilter : QuestFilter
{
    public AcceptedQuestFilter()
    {

    }

    public List<QuestStatus> Filter(List<QuestStatus> toFilter)
    {
        return toFilter.Where(x => x.Progression.CurrentState == QuestProgression.State.Accepted).ToList();
    }
}

public class FinishedQuestFilter : QuestFilter
{
    public FinishedQuestFilter()
    {

    }

    public List<QuestStatus> Filter(List<QuestStatus> toFilter)
    {
        return toFilter.Where(x => x.Progression.CurrentState == QuestProgression.State.Finished).ToList();
    }
}

public class NonFinishedQuestFilter : QuestFilter
{
    public NonFinishedQuestFilter()
    {

    }

    public List<QuestStatus> Filter(List<QuestStatus> toFilter)
    {
        return toFilter.Where(x => x.Progression.CurrentState != QuestProgression.State.Finished).ToList();
    }
}
