﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SerializedQuestStatus
{
    public int ID;
    public QuestProgression Progression;

    public SerializedQuestStatus(QuestStatus status)
    {
        ID = status.Quest.Id;
        Progression = status.Progression;
    }
}

public struct QuestProgressEvent
{
    public enum EventType
    {
        Talk, Search, Kill, Explore, Tutorial
    }

    public EventType Type;
    public int ID;

    public QuestProgressEvent(EventType type, int id)
    {
        Type = type;
        ID = id;
    }
}

public struct QuestEvent
{
    public enum EventType
    {
        Started, Completed, Finished, Abandoned
    }

    public EventType Type;
    public Quest Quest;

    public QuestEvent(EventType type, Quest quest)
    {
        Type = type;
        Quest = quest;
    }
}

public class QuestManager : PersistentSingleton<QuestManager>, EventListener<QuestProgressEvent>
{
    public List<QuestStatus> Quests { get; set; }

    protected override void Awake()
    {
        base.Awake();
        
        Quests = new List<QuestStatus>();
    }
    
    void OnEnable()
    {
        this.EventStartListening<QuestProgressEvent>();
    }
    
    void OnDisable()
    {
        this.EventStopListening<QuestProgressEvent>();
    }
    
    public void LoadQuestManager(List<SerializedQuestStatus> quests)
    {
        foreach (var quest in quests)
        {
            Quests.Add(new QuestStatus(QuestDB.Instance.GetById(quest.ID), quest.Progression));
        }
    }

    public QuestStatus GetQuest(int ID)
    {
        return Quests.Find(x => x.Quest.Id == ID);
    }

    public bool QuestCompleted(Quest quest)
    {
        List<QuestStatus> finished = new CompletedQuestFilter().Filter(Quests);

        return finished.Exists(x => x.Quest.Id == quest.Id);
    }
    
    public bool QuestFinished(Quest quest)
    {
        List<QuestStatus> finished = new FinishedQuestFilter().Filter(Quests);

        return finished.Exists(x => x.Quest.Id == quest.Id);
    }

    public bool QuestAccepted(Quest quest)
    {
        List<QuestStatus> accepted = new AcceptedQuestFilter().Filter(Quests);

        return accepted.Exists(x => x.Quest.Id == quest.Id);
    }
    
    public bool QuestAvailable(Quest quest)
    {
        if (Quests.Exists(x => x.Quest.Id == quest.Id))
        {
            return false;
        }

        return CheckPreviousQuestConditions(quest);
    }
    
    private bool CheckPreviousQuestConditions(Quest quest)
    {
        List<QuestStatus> finished = new FinishedQuestFilter().Filter(Quests);
        foreach (int previousQuestId in quest.QuestConditions.previousQuest)
        {
            if (!finished.Exists(x => x.Quest.Id == previousQuestId))
            {
                return false;
            }
        }

        return true;
    }

    public void AcceptQuest(Quest quest)
    {
        QuestStatus questStatus = Quests.Find(x => quest.Id == x.Quest.Id);

        if (questStatus == null)
        {
            questStatus = new QuestStatus(quest);
            Quests.Add(questStatus);
            EventManager.TriggerEvent(new QuestEvent(QuestEvent.EventType.Started, quest));
        }
    }

    public void DeleteQuest(int questID)
    {
        List<QuestStatus> nonfinished = new NonFinishedQuestFilter().Filter(Quests);
        QuestStatus quest = nonfinished.Find(x => x.Quest.Id == questID);

        if (quest != null)
        {
            Quests.Remove(quest);
            EventManager.TriggerEvent(new QuestEvent(QuestEvent.EventType.Abandoned, quest.Quest));
        }
    }

    public void FinishQuest(Quest quest)
    {
        QuestStatus status = GetQuest(quest.Id);

        status.Progression.CurrentState = QuestProgression.State.Finished;
        EventManager.TriggerEvent(new QuestEvent(QuestEvent.EventType.Finished, quest));
    }

    private void KilledEnemy(int enemyID)
    {
        List<QuestStatus> accepted = new AcceptedQuestFilter().Filter(Quests);
        accepted.ForEach(x => x.UpdateKillGoals(enemyID));
    }
    
    private void TutorialAction(int actionID)
    {
        List<QuestStatus> accepted = new AcceptedQuestFilter().Filter(Quests);
        accepted.ForEach(x => x.UpdateTutorialGoals(actionID));
    }
    
    private void ActivateNPC(int npcID)
    {
        List<QuestStatus> accepted = new AcceptedQuestFilter().Filter(Quests);
        accepted.ForEach(x => x.UpdateNPCGoals(npcID));
    }
    
    private void ActivateZone(int zoneID)
    {
        List<QuestStatus> accepted = new AcceptedQuestFilter().Filter(Quests);
        accepted.ForEach(x => x.UpdateZoneGoals(zoneID));
    }
    
    public void OnEvent(QuestProgressEvent eventType)
    {
        switch (eventType.Type)
        {
            case QuestProgressEvent.EventType.Talk:
                ActivateNPC(eventType.ID);
                break;
            case QuestProgressEvent.EventType.Search:
                ActivateZone(eventType.ID);
                break;
            case QuestProgressEvent.EventType.Kill:
                KilledEnemy(eventType.ID);
                break;
            case QuestProgressEvent.EventType.Explore:
                ActivateZone(eventType.ID);
                break;
            case QuestProgressEvent.EventType.Tutorial:
                TutorialAction(eventType.ID);
                break;
        }
    }
}
