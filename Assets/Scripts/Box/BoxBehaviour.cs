﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoxBehaviour : MonoBehaviour
{
    public Collider Collider { get; set; }
    public GameObject BaseGameObject { get; set; }
    
    protected virtual void Awake()
    {
        BaseGameObject = transform.parent.gameObject;
        Collider = GetComponent<Collider>();
    }
    
    public void EnableBox(bool enable)
    {
        enabled = enable;
        Collider.enabled = enable;
    }
}
