﻿using System.Linq;
using UnityEngine;

public class DefenseboxBehaviour : BoxBehaviour
{
   private DefenseTechnique m_Technique;
   
   public Agent Agent { get; set; }

   protected override void Awake()
   {
      base.Awake();

      Agent = BaseGameObject.GetComponent<Agent>();
   }

   private void Update()
   {
      Collider[] hits = Physics.OverlapBox(Collider.bounds.center, Collider.bounds.extents, Collider.transform.rotation, LayerMask.GetMask("Hitbox"));

      foreach (var collision in hits)
      {
         HitboxBehaviour hitbox = collision.GetComponent<HitboxBehaviour>();
         HitTechnique hit = hitbox.Technique as HitTechnique;

         if (hit.Damage.Target.Contains(BaseGameObject.tag))
         {
            HitboxHit(collision, hit);
         }
      }
   }
   
   public void HitboxHit(Collider hitbox, HitTechnique technique)
   {
      m_Technique.HitboxHit(hitbox, technique);
   }
   
   public void SetTechnique(DefenseTechnique technique)
   {
      m_Technique = technique;
   }
}
