﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Timer : MonoBehaviour
{
    [Tooltip("Tiempo inicial en segundos")]
    public int tiempoInicial;

    [Tooltip("Escala del tiempo del reloj")]
    public float escalaDelTiempo = 1;

    private TextMeshProUGUI myText;
    private float tiempoDelFrameConTimeScale = 0f;
    private float tiempoAMostrarEnSegundos = 0f;

    private float escalaTiempoPausar, escalaTiempoInicial;
    private bool estaPausado = false;



    // Start is called before the first frame update
    void Start()
    {
        escalaTiempoInicial = escalaDelTiempo;
        myText = GetComponent<TextMeshProUGUI>();
        tiempoAMostrarEnSegundos = tiempoInicial;
        ActualizarReloj(tiempoInicial);
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDelFrameConTimeScale = Time.deltaTime * escalaDelTiempo;
        tiempoAMostrarEnSegundos += tiempoDelFrameConTimeScale;
        ActualizarReloj(tiempoAMostrarEnSegundos);
    }
    public void ActualizarReloj(float tiempoEnSegundos)
    {
        //Asegurar que el tiempo no sea negativo
        if (tiempoEnSegundos < 0) tiempoEnSegundos = 0;
        //Ya

        int horas = 0;
        int minutos = 0;
        int segundos = 0;
        string textoDelReloj;

        //Calcular Horas, minutos y segundos
        horas = (int)tiempoEnSegundos / 3600;
        minutos = (int)(tiempoEnSegundos - (horas * 3600)) / 60;
        segundos = (int)tiempoEnSegundos % 60;

        if (horas >= 24) horas = 0;

        textoDelReloj = horas.ToString("00") + ":" + minutos.ToString("00");

        //Actualizar el elemento de text de UIcon la cadena de caracteres
        myText.text = textoDelReloj;
    }
}

