﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using Rewired.Integration.UnityUI;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIPause : Singleton<GUIPause>
{
    public CanvasGroup PauseCanvasGroup;
    public CanvasGroup OptionsCanvasGroup;
    public CanvasGroup ControlsCanvasGroup;
    
    public GameObject FirstGameObject;
    public GameObject FirstControlsSelectable;
    
    public CanvasGroup PS4Controls;
    public CanvasGroup PCControls;
    
    private CanvasGroup m_MenuCanvasGroup;

    protected override void Awake()
    {
        base.Awake();
        
        m_MenuCanvasGroup = GetComponent<CanvasGroup>();
    }

    public void OpenMenu()
    {
        m_MenuCanvasGroup.alpha = 1f;
        PauseCanvasGroup.alpha = 1f;
        PauseCanvasGroup.interactable = true;
        PauseCanvasGroup.blocksRaycasts = true;
        m_MenuCanvasGroup.interactable = true;
        m_MenuCanvasGroup.blocksRaycasts = true;
        (EventSystem.current.currentInputModule as RewiredStandaloneInputModule).InitModuleInput(FirstGameObject);
    }

    public void CloseMenu()
    {
        EventSystem.current.currentInputModule.DeactivateModule();
        m_MenuCanvasGroup.alpha = 0f;
        m_MenuCanvasGroup.interactable = false;
        m_MenuCanvasGroup.blocksRaycasts = false;
        PauseCanvasGroup.alpha = 0f;
        PauseCanvasGroup.interactable = false;
        PauseCanvasGroup.blocksRaycasts = false;  
        OptionsCanvasGroup.alpha = 0f;
        OptionsCanvasGroup.interactable = false;
        OptionsCanvasGroup.blocksRaycasts = false;
        ControlsCanvasGroup.alpha = 0f;
        ControlsCanvasGroup.interactable = false;
        ControlsCanvasGroup.blocksRaycasts = false;
        GUIInGameSettings.Instance.UnPausedMenu();
    }

    public void ClosePauseMenu()
    {
        GUIManager.Instance.ClosePauseMenu();
    }
    
    public void SaveGame()
    {
        EventManager.TriggerEvent(new GameEvent(GameEvents.SAVE_GAME));
    }
    
    public void OpenControlsPanel()
    {
        ControlsCanvasGroup.alpha = 1;
        ControlsCanvasGroup.interactable = true;
        ControlsCanvasGroup.blocksRaycasts = true;
        PauseCanvasGroup.alpha = 0;
        PauseCanvasGroup.interactable = false;
        PauseCanvasGroup.blocksRaycasts = false;
        switch (GameManager.GetControllerType())
        {
            case ControllerType.Joystick:
                PS4Controls.alpha = 1;
                PCControls.alpha = 0;
                break;
            case ControllerType.Keyboard:
                PCControls.alpha = 1;
                PS4Controls.alpha = 0;
                break;
            case ControllerType.Mouse:
                PCControls.alpha = 1;
                PS4Controls.alpha = 0;
                break;
        }
        EventSystem.current.SetSelectedGameObject(FirstControlsSelectable);
    }
    
    public void CloseControlsPanel()
    {
        ControlsCanvasGroup.alpha = 0;
        ControlsCanvasGroup.interactable = false;
        ControlsCanvasGroup.blocksRaycasts = false;
        PauseCanvasGroup.alpha = 1;
        PauseCanvasGroup.interactable = true;
        PauseCanvasGroup.blocksRaycasts = true;
        EventSystem.current.SetSelectedGameObject(FirstGameObject);
    }

    public void ExitToMenu()
    {
        GameManager.Instance.UnPause();
        MatchManager.Instance.ExitMatch();
    }
}
