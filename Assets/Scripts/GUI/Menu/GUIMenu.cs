﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIMenu : MonoBehaviour
{
    [Header("First Selectables")]
    public Button LoadSavedGame;
    public Button StartNewGame;
    public TMP_InputField InputPlayerName;

    [Header("Panels")]
    public CanvasGroup OptionsPanel;
    public CanvasGroup PlayerPanel;

    [Header("Scene")]
    [Scene] 
    public string NextScene;

    private bool m_HasSavedGame;
    
    void Start()
    {
        if (SaveLoadManager.SaveExists(GameManager.SAVE_FILE_NAME, GameManager.SAVE_FOLDER_NAME)) m_HasSavedGame = true;
        else m_HasSavedGame = false;
        SoundManager.Instance.PlayBackgroundMusic(SoundManager.Instance.MenuAudioClip);
        Initialize();
    }

    public void Initialize()
    {
        if (m_HasSavedGame)
        {
            LoadSavedGame.gameObject.SetActive(true);
            EventSystem.current.SetSelectedGameObject(LoadSavedGame.gameObject);
        }
        else
        {
            LoadSavedGame.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(StartNewGame.gameObject);
        }
    }
    
    public void NewGame()
    {
        GameManager.Instance.PlayerName = InputPlayerName.text;
        LoadingSceneManager.Instance.LoadNewScene(NextScene);
    }

    public void LoadGame()
    {
        GameManager.Instance.LoadGame();
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
    
    public void OpenOptionsPanel()
    {
        OptionsPanel.alpha = 1;
        OptionsPanel.interactable = true;
        OptionsPanel.blocksRaycasts = true;
        PlayerPanel.alpha = 0;
        PlayerPanel.interactable = false;
        PlayerPanel.blocksRaycasts = false;
    }

    public void OpenPlayerPanel()
    {
        PlayerPanel.alpha = 1;
        PlayerPanel.interactable = true;
        PlayerPanel.blocksRaycasts = true;
        OptionsPanel.alpha = 0;
        OptionsPanel.interactable = false;
        OptionsPanel.blocksRaycasts = false;
        EventSystem.current.SetSelectedGameObject(InputPlayerName.gameObject);
    }
}
