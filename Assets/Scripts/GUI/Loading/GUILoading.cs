﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUILoading : Singleton<GUILoading>
{
    [Header("Text")]
    public Image Loading;
    public TextMeshProUGUI Confirm;
    public Image PCControls;
    public Image PS4Controls;
    public GameObject Camera;
    
    public void Init()
    {
        switch (GameManager.GetControllerType())
        {
            case ControllerType.Keyboard:
                PCControls.gameObject.SetActive(true);
                break;
            case ControllerType.Mouse:
                PCControls.gameObject.SetActive(true);
                break;
            case ControllerType.Joystick:
                PS4Controls.gameObject.SetActive(true);
                break;
        }
    }
    
    public void LoadingReady()
    {
        Loading.gameObject.SetActive(false);
        Confirm.gameObject.SetActive(true);
    }
    
    public void Submit()
    {
        Confirm.gameObject.SetActive(false);
    }
    
    public void RemoveCamera()
    {
        Camera.SetActive(false);
    }
}
