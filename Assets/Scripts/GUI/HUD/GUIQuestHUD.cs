﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIQuestHUD : MonoBehaviour
{
    public TextMeshProUGUI Title;
    
    [Header("Layout")]
    public RectTransform ProgressLayout;

    [Header("Prefab")]
    public GameObject ProgressPrefab;

    public QuestStatus Quest { get; set; }

    public void Init(QuestStatus quest)
    {
        Quest = quest;
        
        Title.text = LocalizationManager.Instance["QUEST", quest.Quest.QuestNameTextKey];
        
        DisplayProgress();
    }
    
    private void DisplayProgress()
    {
        foreach (var goal in Quest.Progression.Goals)
        {
            GUIQuestProgress progress = Instantiate(ProgressPrefab, ProgressLayout).GetComponent<GUIQuestProgress>();
            CheckState(progress);
            progress.Init(goal);
        }
    }
    
    private void CheckState(GUIQuestProgress progress)
    {
        if (Quest.Progression.CurrentState == QuestProgression.State.Completed)
        {
            progress.SliderFill.color = new Color(1f,0.64f,0f,1f);
        }
        else
        {
            Title.fontStyle = FontStyles.Bold;
        }
    }
}
