﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour
{
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Text;

    public GameObject AnswerPrefab;

    public Button NextBtn;
    public Button HiddenBtn;
    public RectTransform Options;
    
    protected Coroutine m_TypeCo;
    protected string m_CurrentTypingText;
    private CanvasGroup m_CanvasGroup;
    
    private void Awake()
    {
        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    public void Clear()
    {
        Text.text = string.Empty;
        HiddenBtn.gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(HiddenBtn.gameObject);
        
        NextBtn.gameObject.SetActive(false);

        for (int i = 0; i < Options.childCount; i++)
        {
            Destroy(Options.GetChild(i).gameObject);
        }
    }
    
    public void SetSpeakerName(string name)
    {
        Name.text = name;
    }
    
    public void Type(string text)
    {
        float charTime = 0.01f;

        m_TypeCo = StartCoroutine(TypeCo(text, charTime));
    }
    
    public void Type(string text, float typingTime)
    {
        float charTime = typingTime / text.Length;

        m_TypeCo = StartCoroutine(TypeCo(text, charTime));
    }
    
    public virtual IEnumerator TypeCo(string text, float charTime)
    {
        Clear();
        Text.text = "";

        m_CurrentTypingText = text;

        DialogueManager.Instance.State.State = DialogueInfo.DialogueState.Typing;

        for (int i = 0; i < text.Length; i++)
        {
            Text.text += text[i];
            yield return new WaitForSeconds(charTime);
        }

        m_CurrentTypingText = string.Empty;

        DialogueManager.Instance.State.State = DialogueInfo.DialogueState.TypingEnd;
    }
    
    public virtual void InstantType(string text)
    {
        Clear();
        Text.text = text;

        DialogueManager.Instance.State.State = DialogueInfo.DialogueState.TypingEnd;
    }
    
    public virtual void SkipTypingText()
    {
        if (m_TypeCo != null)
        {
            StopCoroutine(m_TypeCo);
            m_TypeCo = null;
        }

        Clear();
        Text.text = m_CurrentTypingText;

        DialogueManager.Instance.State.State = DialogueInfo.DialogueState.TypingEnd;
    }

    public void Show()
    {
        m_CanvasGroup.alpha = 1f;
        m_CanvasGroup.interactable = true;
    }

    public void Hide()
    {
        m_CanvasGroup.alpha = 0f;
        m_CanvasGroup.interactable = false;
    }
    
    public void AddOptions(List<string> answers)
    {
        foreach (var answer in answers)
        {
            TextMeshProUGUI text = Instantiate(AnswerPrefab, Options).GetComponentInChildren<TextMeshProUGUI>();
            text.text = LocalizationManager.Instance["DIALOGUE", answer];
        }
        HiddenBtn.gameObject.SetActive(false);

        EventSystem.current.SetSelectedGameObject(Options.GetChild(0).gameObject);
    }
    
    public void ShowNextIcon()
    {
        HiddenBtn.gameObject.SetActive(false);

        NextBtn.gameObject.SetActive(true);
        EventSystem.current.SetSelectedGameObject(NextBtn.gameObject);
    }
    
    public virtual void AnswerDialogue(int index = 0)
    {
        Clear();

        DialogueManager.Instance.ExecuteNextNode(index);
    }
}
