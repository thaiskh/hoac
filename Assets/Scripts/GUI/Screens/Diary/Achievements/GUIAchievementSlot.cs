﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIAchievementSlot : MonoBehaviour
{
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Description;
    public Image Image;
    
    public AchievementStatus Achievement { get; set; }
    
    public void Init(AchievementStatus achievement)
    {
        Achievement = achievement;

        Title.text = LocalizationManager.Instance["ACHIEVEMENT", achievement.Achievement.AchievementNameTextKey];
        Description.text = LocalizationManager.Instance["ACHIEVEMENT", achievement.Achievement.AchievementDescriptionTextKey];
        Image.sprite = Achievement.Achievement.AchievementImage;
    }
}
