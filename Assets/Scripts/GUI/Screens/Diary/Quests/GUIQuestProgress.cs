﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIQuestProgress : MonoBehaviour
{
    public SliderBar Slider;
    public Image SliderFill;
    public TextMeshProUGUI Progress;
   
    public ProgressGoal Goal { get; set; }

    public void Init(ProgressGoal goal)
    {
        Goal = goal;

        if (Goal is KillProgressGoal)
        {
            KillProgressGoal kill = (Goal as KillProgressGoal);
            Progress.text = kill.Current + "/" + kill.Required + " " + kill.Name;
            Slider.UpdateBar(kill.Current, 0, kill.Required);
        } 
        else if (Goal is ActivateNPCProgressGoal)
        {
            ActivateNPCProgressGoal npc = (Goal as ActivateNPCProgressGoal);
            if (!npc.Finished)
            {
                Progress.text = "0/1 " + npc.Name;
                Slider.UpdateBar(0, 0, 1);
            }
            else
            {
                Progress.text = "1/1 " + npc.Name;
                Slider.UpdateBar(1, 0, 1);
            }
        } 
        else if (Goal is ActivateZoneProgressGoal)
        {
            ActivateZoneProgressGoal zone = (Goal as ActivateZoneProgressGoal);
            if (!zone.Finished)
            {
                Progress.text = "0/1 " + zone.Name;
                Slider.UpdateBar(0, 0, 1);
            }
            else
            {
                Progress.text = "1/1 " + zone.Name;
                Slider.UpdateBar(1, 0, 1);
            }
        }
        else if (Goal is TutorialProgressGoal)
        {
            TutorialProgressGoal action = (Goal as TutorialProgressGoal);
            if (!action.Finished)
            {
                Progress.text = "0/1 " + action.Name;
                Slider.UpdateBar(0, 0, 1);
            }
            else
            {
                Progress.text = "1/1 " + action.Name;
                Slider.UpdateBar(1, 0, 1);
            }
        }
    }
}
