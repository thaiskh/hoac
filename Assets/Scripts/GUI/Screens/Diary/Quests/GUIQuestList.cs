﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIQuestList : MonoBehaviour
{
    private const int MAX_QUESTS_PER_PAGE = 4;
    
    public RectTransform Layout;
    public GameObject QuestPrefab;
    public int Page;

    public void DisplayContent(List<QuestStatus> quests)
    {
        int iterator = 0;
        switch (Page)
        {
            case 1:
                iterator = 0;
                break;
            case 2:
                iterator += 4;
                break;
            case 3:
                iterator += 8;
                break;
            case 4:
                iterator += 12;
                break;
            case 5:
                iterator += 16;
                break;
            case 6:
                iterator += 20;
                break;
            default:
                iterator = 0;
                break;
        }

        int max = iterator + MAX_QUESTS_PER_PAGE;
        for (int i = iterator; i < quests.Count && i < max; i++)
        {
            var quest = quests[i];
            GUIQuestSlot slot = Instantiate(QuestPrefab, Layout).GetComponent<GUIQuestSlot>();
            slot.Init(quest);
        }
    }

    public void EraseContent()
    {
        foreach (RectTransform child in Layout)
        {
            Destroy(child.gameObject);
        }
        Layout.DetachChildren();
    }
}
