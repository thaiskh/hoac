﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;

public class GUIShopModal : GUIBooleanModal
{
    private NumberFormatInfo m_NumberFormatter;

    void Awake()
    {
        m_NumberFormatter = new NumberFormatInfo
        {
            NumberGroupSeparator = ","
        };
    }

    public override void Init(string question, string yes, string no, UnityAction yesEvent, UnityAction noEvent, params object[] additional)
    {
        Item item = (Item)additional[0];
        int quantity = (int)additional[1];

        question = question.Replace("%replace%", LocalizationManager.Instance["ITEM", item.Name]);
        question = question.Replace("%quantity%", quantity.ToString());
        question = question.Replace("%price%", (quantity * (item as IBuy).BuyPrice).ToString("N0", m_NumberFormatter));

        base.Init(question, yes, no, yesEvent, noEvent, additional);
    }
}
