﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIShop : MonoBehaviour, ICancelHandler, ISelectHandler, IMoveHandler
{
    public ShopOption ShopOptionPrefab;
    public GameObject OverlayMessageScreen;
    
    [Header("UI Elements")]
    public TextMeshProUGUI ShopName;
    public TextMeshProUGUI Category;
    public RectTransform Content;
    public TextMeshProUGUI Coins;
    public TextMeshProUGUI Description;
    public Image ItemIcon;
    public TextMeshProUGUI Price;
    public TextMeshProUGUI Quantity;
    public RectTransform QuantityArrows;
    
    [Header("Modal")]
    public GameObject ConfirmationModal;
    
    public Shop Shop { get; set; }
    public Transform Opener { get; set; }

    private Shop.Category m_SelectedCategory;
    private NumberFormatInfo m_NumberFormatter;
    private GameObject m_TmpOverlayMessageScreen;

    private void Awake()
    {
        m_NumberFormatter = new NumberFormatInfo()
        {
            NumberGroupSeparator = ","
        };
    }

    public void OpenShop(Shop shop, Transform opener)
    {
        Shop = shop;
        Opener = opener;

        Coins.text = MatchManager.Instance.CurrentCoins.ToString("N0", m_NumberFormatter);
        ShopName.text = LocalizationManager.Instance["SHOP", Shop.ShopName];

        DrawContent(Shop.Category.All);
    }

    protected void DrawContent(Shop.Category category)
    {
        Category.text = LocalizationManager.Instance["SHOP", "CATEGORY_" + category.ToString().ToUpper()];
        List<ShopItem> items = Shop.GetShopItemsCategory(category);

        for (int i = 0; i < items.Count; i++)
        {
            ShopOption option = Instantiate(ShopOptionPrefab, Content);
            option.Init(items[i]);
        }

        if (Content.childCount > 0)
        {
            EventSystem.current.SetSelectedGameObject(Content.GetChild(0).gameObject);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(gameObject);
        }
    }

    protected void EraseContent()
    {
        for (int i = Content.childCount - 1; i >= 0; i--)
        {
            Transform child = Content.GetChild(i);
            
            child.SetParent(null);
            Destroy(child.gameObject);
        }
    }

    public void ReloadContentNewCategory()
    {
        EraseContent();
        DrawContent(m_SelectedCategory);
    }

    public void ReloadContent()
    {
        for (int i = Content.childCount - 1; i >= 0; i--)
        {
            ShopOption child = Content.GetChild(i).GetComponent<ShopOption>();
            if (child.Item.Quantity > 0)
            {
                child.Refresh();
            }
            else
            {
                child.transform.SetParent(null);
                Destroy(child.gameObject);
                if (i > 0)
                {
                    i--;
                }

                if (i < Content.childCount)
                {
                    EventSystem.current.SetSelectedGameObject(Content.GetChild(i).gameObject);
                }
                else
                {
                    EventSystem.current.SetSelectedGameObject(gameObject);
                }
            }
        }
        Coins.text = MatchManager.Instance.CurrentCoins.ToString("N0", m_NumberFormatter);
    }
    
    public void NextCategory()
    {
        m_SelectedCategory++;

        if (m_SelectedCategory > Shop.Category.Usable)
        {
            m_SelectedCategory = Shop.Category.All;
        }

        ReloadContentNewCategory();
    }
    
    public void PreviousCategory()
    {
        m_SelectedCategory--;

        if (m_SelectedCategory < Shop.Category.All)
        {
            m_SelectedCategory = Shop.Category.Usable;
        }

        ReloadContentNewCategory();
    }

    public void OnCancel(BaseEventData eventData)
    {
        GUIManager.Instance.CloseShopScreen();
    }

    public void OnSelect(BaseEventData eventData)
    {
        Description.text = string.Empty;
        Price.text = string.Empty;
        Quantity.text = string.Empty;

        ItemIcon.sprite = null;
        ItemIcon.color = new Color(ItemIcon.color.r,ItemIcon.color.g,ItemIcon.color.b, 0f);
    }

    public void OnMove(AxisEventData eventData)
    {
        if (eventData.moveDir == MoveDirection.Left)
        {
            PreviousCategory();
        }
        else if (eventData.moveDir == MoveDirection.Right)
        {
            NextCategory();
        }
    }
    
    public void ShowOverlayMessage(string message, TextAnchor anchor,GameObject previousObject)
    {
        if (m_TmpOverlayMessageScreen != null)
        {
            Destroy(m_TmpOverlayMessageScreen);
        }

        m_TmpOverlayMessageScreen = Instantiate(OverlayMessageScreen, transform);
        m_TmpOverlayMessageScreen.GetComponentInChildren<OverlayMessageSystem>().NewMessage(message, anchor, previousObject);
    }
}
