﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopOption : Selectable, ISubmitHandler, ICancelHandler, IMoveHandler, ISelectHandler, IPointerExitHandler, IPointerEnterHandler
{
   public enum State
   {
      Selection,
      Quantity
   }

   public TextMeshProUGUI Name;
   public TextMeshProUGUI Quantity;
   
   public ShopItem Item { get; set; }

   private GUIShop m_GUIShop;
   private State CurrentState;
   private int m_CurrentQuantity;

   private NumberFormatInfo m_NumberFormatter;

   protected override void Awake()
   {
      base.Awake();

      m_NumberFormatter = new NumberFormatInfo()
      {
         NumberGroupSeparator = ","
      };

      m_GUIShop = GetComponentInParent<GUIShop>();
   }

   public void Init(ShopItem item)
   {
      Item = item;

      Name.text = LocalizationManager.Instance["ITEM", item.Item.Name];
      Quantity.text = item.Quantity.ToString();

      m_CurrentQuantity = 1;
   }

   public override void OnMove(AxisEventData eventData)
   {
      if (CurrentState == State.Selection)
      {
         if (eventData.moveDir == MoveDirection.Left)
         {
            m_GUIShop.PreviousCategory();
         }
         else if (eventData.moveDir == MoveDirection.Right)
         {
            m_GUIShop.NextCategory();
         }
         else
         {
            base.OnMove(eventData);
         }
      } 
      else if (CurrentState == State.Quantity)
      {
         if (eventData.moveDir == MoveDirection.Left)
         {
            if (m_CurrentQuantity > 1)
            {
               m_CurrentQuantity--;

               m_GUIShop.Price.text = (m_CurrentQuantity * (Item.Item as IBuy).BuyPrice).ToString("N0", m_NumberFormatter);
               m_GUIShop.Quantity.text = m_CurrentQuantity.ToString();
            }                    
         }
         else if(eventData.moveDir == MoveDirection.Right)
         {
            if (Item.Quantity > m_CurrentQuantity)
            {
               ListedItem inventoryListedItem = Inventory.Instance.FindItem(Item.Item);
               int inventoryQuantity = inventoryListedItem == null ? 0 : inventoryListedItem.Quantity;

               if (inventoryQuantity + m_CurrentQuantity <= Item.Item.MaxQuantity)
               {
                  m_CurrentQuantity++;

                  m_GUIShop.Price.text = (m_CurrentQuantity * (Item.Item as IBuy).BuyPrice).ToString("N0", m_NumberFormatter);
                  m_GUIShop.Quantity.text = m_CurrentQuantity.ToString();
               }
            } 
         }
      }
   }

   public void Refresh()
   {
      Name.text = LocalizationManager.Instance["ITEM", Item.Item.ItemName];
      Quantity.text = Item.Quantity.ToString();
   }
   
   public void OnCancel(BaseEventData eventData)
   {
      if(CurrentState == State.Selection)
      {
         GUIManager.Instance.CloseShopScreen();
      }
      else if(CurrentState == State.Quantity)
      {
         ChangeToState(State.Selection);
      }
   }
   
   public void OnSort()
   {
      if(CurrentState == State.Selection)
      {
         Inventory.Instance.Sort();
         m_GUIShop.ReloadContent();
      }
   }
   
   public override void OnSelect(BaseEventData eventData)
   {
      base.OnSelect(eventData);

      m_CurrentQuantity = 1;

      m_GUIShop.Quantity.text = m_CurrentQuantity.ToString();

      m_GUIShop.Description.text = Item.Item.GetDescription();

      m_GUIShop.ItemIcon.sprite = Item.Item.Icon;
      m_GUIShop.ItemIcon.color = new Color(1f, 1f, 1f, 1f);

      m_GUIShop.Price.text = (m_CurrentQuantity * (Item.Item as IBuy).BuyPrice).ToString("N0", m_NumberFormatter);
   }

   public void OnSubmit(BaseEventData eventData)
   {
      if (CurrentState == State.Selection)
      {
         ChangeToState(State.Quantity);
      } 
      else if (CurrentState == State.Quantity)
      {
         if (MatchManager.Instance.CurrentCoins >= (Item.Item as IBuy).BuyPrice * m_CurrentQuantity)
         {
            ListedItem inventoryListedItem = Inventory.Instance.FindItem(Item.Item);
            int inventoryQuantity = inventoryListedItem == null ? 0 : inventoryListedItem.Quantity;

            if (inventoryQuantity + m_CurrentQuantity <= Item.Item.MaxQuantity)
            {
               GUIShopModal modal = Instantiate(m_GUIShop.ConfirmationModal, m_GUIShop.transform).GetComponent<GUIShopModal>();

               string question = LocalizationManager.Instance["SHOP", "BUY_CONFIRMATION"];
               string yes = LocalizationManager.Instance["GUI", "YES"];
               string no = LocalizationManager.Instance["GUI", "NO"];

               modal.Init(question, yes, no, BuyItem, CancelBuy, Item.Item, m_CurrentQuantity);
               ChangeToState(State.Selection);
            }
            else
            {
               m_GUIShop.ShowOverlayMessage(LocalizationManager.Instance["SHOP", "NO_MORE_SPACE"], TextAnchor.MiddleCenter, gameObject);
               ChangeToState(State.Selection);
            }
         }
         else
         {
            m_GUIShop.ShowOverlayMessage(LocalizationManager.Instance["SHOP", "NO_COINS"], TextAnchor.MiddleCenter, gameObject);
            ChangeToState(State.Selection);
         }
      }
   }

   public void BuyItem()
   {
      EventManager.TriggerEvent(new CoinsEvent(CoinsEvent.CoinsMethods.Remove, (Item.Item as IBuy).BuyPrice * m_CurrentQuantity));
      Inventory.Instance.AddItem(Item.Item, m_CurrentQuantity);
      m_GUIShop.Shop.RemoveItem(Item, m_CurrentQuantity);

      m_GUIShop.ReloadContent();

      CurrentState = State.Selection;
      m_GUIShop.QuantityArrows.gameObject.SetActive(false);

      if (Item.Quantity > 0)
      {
         EventSystem.current.SetSelectedGameObject(gameObject);
      }
   }
   
   private void ChangeToState(State nextState)
   {
      if(nextState == CurrentState)
      {
         return;
      }

      CurrentState = nextState;
      switch (nextState)
      {
         case State.Quantity:
            m_GUIShop.QuantityArrows.gameObject.SetActive(true);
            break;
         case State.Selection:
            m_GUIShop.QuantityArrows.gameObject.SetActive(false);
            break;
      }
   }
   
   public void CancelBuy()
   {
      ChangeToState(State.Selection);
      EventSystem.current.SetSelectedGameObject(gameObject);
   }
   
   public override void OnPointerExit(PointerEventData eventData)
   {
      if (EventSystem.current.currentSelectedGameObject != null)
      {
         if (EventSystem.current.currentSelectedGameObject.GetComponent<ShopOption>().CurrentState == State.Quantity)
         {
            return;
         }
      }
      base.OnPointerExit(eventData);
      EventSystem.current = null;
   }

   public override void OnPointerEnter(PointerEventData eventData)
   {
      if(EventSystem.current.currentSelectedGameObject != null)
      {
         if (EventSystem.current.currentSelectedGameObject.GetComponent<ShopOption>().CurrentState == State.Quantity)
         {
            return;
         }
      }
      base.OnPointerEnter(eventData);
      EventSystem.current.SetSelectedGameObject(gameObject);
   }
}
