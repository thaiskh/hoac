﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMovement : AgentMovement
{
    public float MovementSpeedMultiplier { get; set; }
    
    public float HorizontalMove { get; set; }
    public float VerticalMove { get; set; }
    public bool IsRunning { get; set; }
    
    public bool IsOnStealth { get; set; }

    private int m_OldSpeed;

    protected override void Start()
    {
        base.Start();

        MovementSpeedMultiplier = 1f;

        if (m_Agent is Enemy)
        {
            m_OldSpeed = m_Agent.Data.Stats.WalkingSpeed;
        }
    }

    protected override bool EvaluateConditions()
    {
        if (m_Agent.FreedomState.CurrentState == AgentStates.AgentMovementConditions.Immobilized)
        {
            return false;
        }

        return true;
    }

    protected override void EvaluateState()
    {
        if ((m_Controller.State.IsGrounded) && ((HorizontalMove != 0) || (VerticalMove != 0)) && (!IsRunning) && (!IsOnStealth) &&
            ((m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Idle) || (m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Running)))
        {
            m_Agent.MovementState.ChangeState(AgentStates.MovementStates.Walking);
        }
        
        if ((m_Controller.State.IsGrounded) && (HorizontalMove != 0 || VerticalMove != 0) && (!IsOnStealth) && (IsRunning))
        {
            m_Agent.MovementState.ChangeState(AgentStates.MovementStates.Running);
        }
        
        if ((m_Controller.State.IsGrounded) && (HorizontalMove != 0 || VerticalMove != 0) && (!IsRunning) && (IsOnStealth))
        {
            m_Agent.MovementState.ChangeState(AgentStates.MovementStates.Stealth);
        }
        
        if ((m_Controller.State.IsGrounded) && (HorizontalMove == 0) && (VerticalMove == 0) && (!IsRunning) && (!IsOnStealth))
        {
            m_Agent.MovementState.ChangeState(AgentStates.MovementStates.Idle);
        }

        if (!m_Controller.State.IsGrounded && ((m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Walking) ||
                                               (m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Idle) || (m_Agent.MovementState.CurrentState == AgentStates.MovementStates.Running)))
        {
            m_Agent.MovementState.ChangeState(AgentStates.MovementStates.Falling);
        }
        
    }

    protected override void EvaluateMovement()
    {
        switch (m_Agent.FreedomState.CurrentState)
        {
            case AgentStates.AgentMovementConditions.Free:
                CalculateDirection();
                if (m_Agent is Enemy)
                {
                    m_Agent.Data.Stats.WalkingSpeed = m_OldSpeed;
                }
                break;
            case AgentStates.AgentMovementConditions.Immobilized:
                if (m_Agent is Enemy)
                {
                    m_Agent.Data.Stats.WalkingSpeed = 0;
                }
                HorizontalMove = 0f;
                VerticalMove = 0f;
                break;
        }
    }

    public virtual void CalculateDirection()
    {
        if (m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Attacking)
        {
            return;
        }
    }

    protected override void CalculateMovementValue()
    {
        float movementFactor = m_Controller.State.IsGrounded
            ? m_Controller.Parameters.SpeedAccelerationOnGround
            : m_Controller.Parameters.SpeedAccelerationOnAir;
        
        float horizontalSpeed = HorizontalMove * MovementSpeed * m_Controller.Parameters.SpeedFactor * MovementSpeedMultiplier;
        float horizontalMovement = Mathf.Lerp(m_Controller.Velocity.x, horizontalSpeed, Time.deltaTime * movementFactor);
        
        float verticalSpeed = VerticalMove * MovementSpeed * m_Controller.Parameters.SpeedFactor * MovementSpeedMultiplier;
        float verticalMovement = Mathf.Lerp(m_Controller.Velocity.z, verticalSpeed, Time.deltaTime * movementFactor);

        m_Controller.SetHorizontalVelocity(horizontalMovement);
        m_Controller.SetVerticalVelocity(verticalMovement);
    }

    public override void Stop()
    {
        NeutralMovement();

        m_Controller.SetHorizontalVelocity(0f);
        m_Controller.SetVerticalVelocity(0f);
    }

    public override void MoveTo(Vector3 position)
    {
        m_Agent.FreedomState.ChangeState(AgentStates.AgentMovementConditions.Fixed);

        HorizontalMove = position.x;
        VerticalMove = position.z;
    }

    public virtual void MoveTo(float horizontalValue, float verticalValue)
    {
        m_Agent.FreedomState.ChangeState(AgentStates.AgentMovementConditions.Fixed);

        HorizontalMove = horizontalValue;
        VerticalMove = verticalValue;
    }
    
    public override void NeutralMovement()
    {
        HorizontalMove = 0f;
        VerticalMove = 0f;
    }
}
