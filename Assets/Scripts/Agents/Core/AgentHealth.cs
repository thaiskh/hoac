﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentHealth : Health
{
    private Agent m_Agent;
    protected Animator m_Animator;

    protected override void Awake()
    {
        base.Awake();

        m_Agent = GetComponent<Agent>();
        m_Animator = GetComponent<Animator>();
    }

    void Update()
    {
        UpdateAnimatorValues();
    }
    public void Initialize()
    {
        UpdateMaximumHealth();
        Restore();
    }

    public void UpdateMaximumHealth()
    {
        MaximumHealth = InfiniteHealth ? int.MaxValue : m_Agent.CurrentStats.Health;

        m_Agent.UpdateHealthUI();
    }

    public override void HealDamage(int healAmmount)
    {
        base.HealDamage(healAmmount);

        m_Agent.UpdateHealthUI();
    }

    public override void Restore()
    {
        base.Restore();

        m_Agent.UpdateHealthUI();
    }

    public override void Restore(float value)
    {
        base.Restore(value);

        m_Agent.UpdateHealthUI();
    }

    protected override int CalculateDamageTaken(int damage)
    {
        float mitigation = m_Agent.CurrentStats.Defense;
        float dmg = damage - (damage * (mitigation / (100 + mitigation)));
        return (int)Mathf.Clamp(dmg, 0f, Mathf.Infinity);
    }

    private void UpdateAnimatorValues()
    {
        m_Animator.SetBool("Damaged", m_Agent.ConditionState.CurrentState == AgentStates.AgentConditions.Damaged);
        m_Animator.SetBool("Dead", m_Agent.ConditionState.CurrentState == AgentStates.AgentConditions.Dead);
    }

}
