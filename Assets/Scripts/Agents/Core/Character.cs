﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CharacterEvent
{
    public enum Type { Death, Revive, RespawnStart, RespawnEnd }

    public Character Character;
    public Type CharacterEventType;

    public CharacterEvent(Character character, Type type)
    {
        Character = character;
        CharacterEventType = type;
    }
}

public struct BlockCharacter
{
    public enum Type { Block, Unblock, BlockMovement }

    public Type Block;

    public BlockCharacter(Type block)
    {
        Block = block;
    }
}

public class Character : Agent, EventListener<LevelingEvents>, EventListener<BlockCharacter>, EventListener<TransitionEvent>
{
    public AudioClip LevelUpSFX;
    
    private GUIIndicator m_CharacterIndicator;

    private Coroutine m_LevelUpCo;

    public bool IsRespawning { get; set; }

    protected override void Awake()
    {
        m_CharacterIndicator = GetComponent<GUIIndicator>();

        base.Awake();
    }
    
    void Start()
    {
        m_CharacterIndicator = UIHUD.Instance.InitIndicator(this);
        UpdateBaseStats();

        m_Health.Initialize();
        m_AgentStamina.Initialize();
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        this.EventStartListening<LevelingEvents>();
        this.EventStartListening<BlockCharacter>();
        this.EventStartListening<TransitionEvent>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        this.EventStopListening<LevelingEvents>();
        this.EventStopListening<BlockCharacter>();
        this.EventStopListening<TransitionEvent>();
    }

    public void Reset()
    {
        MatchManager.Instance.LevelSystem.Reset();
    }

    public void Kill()
    {
        m_Health.Kill();
    }

    public override IEnumerator OnDamaged(Transform damager)
    {
        ConditionState.ChangeState(AgentStates.AgentConditions.Damaged);
        
        if(MovementState.CurrentState == AgentStates.MovementStates.Stealth) ExitStealth();

        yield return new WaitForSeconds(m_Health.DamageDuration);

        if (m_Health.CurrentHealth <= 0)
        {
            yield return StartCoroutine(OnDeath(damager));
        }
        else
        {
            ConditionState.ChangeState(AgentStates.AgentConditions.Normal);
        }
    }

    public override IEnumerator OnDeath(Transform damager)
    {
        ConditionState.ChangeState(AgentStates.AgentConditions.Dead);
        
        if (m_Hitbox != null) m_Hitbox.EnableBox(false);

        if (MatchManager.Instance.AreAllDead)
        {
            yield return new WaitForSeconds(2f);
            
            EventManager.TriggerEvent(new MatchEvent(MatchEvent.MatchEventType.Death));
            Enemy en = damager.GetComponent<Enemy>();
            if(en != null) EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Die,en.Data.Id));
        }
    }

    protected void RestoreCharacterCondition()
    {
        if (m_Hurtbox != null) m_Hurtbox.EnableBox(true);
        
        m_Movement.Stop();

        FreedomState.ChangeState(AgentStates.AgentMovementConditions.Free);
        MovementState.ChangeState(AgentStates.MovementStates.Idle);
    }

    public void Revive(Vector3 position, float life)
    {
        IsRespawning = true;

        RestoreCharacterCondition();

        EventManager.TriggerEvent(new CharacterEvent(this, CharacterEvent.Type.Revive));

        m_Health.Restore(life);
        m_AgentStamina.Restore();

        transform.position = position;
        
        m_Animator.Play("Locomotion");

        ConditionState.ChangeState(AgentStates.AgentConditions.Normal);

        IsRespawning = false;
    }

    public void Restore()
    {
        m_Health.Restore();
        m_AgentStamina.Restore();
    }

    public IEnumerator ReSpawn(Vector3 respawnPosition)
    {
        IsRespawning = true;

        EventManager.TriggerEvent(new CharacterEvent(this, CharacterEvent.Type.RespawnStart));

        yield return new WaitForSeconds(0.5f);

        RestoreCharacterCondition();
        transform.position = respawnPosition;

        yield return new WaitForSeconds(0.5f);

        ConditionState.ChangeState(AgentStates.AgentConditions.Normal);
        EventManager.TriggerEvent(new CharacterEvent(this, CharacterEvent.Type.RespawnEnd));

        IsRespawning = false;
    }

    public void OnEvent(LevelingEvents eventType)
    {
        switch (eventType.Type)
        {
            case LevelingEvents.EventType.LevelUp:
                LevelUp();
                break;
            case LevelingEvents.EventType.Reset:
                ResetLevel();
                break;
        }
    }

    private void ResetLevel()
    {
        UpdateBaseStats();

        m_AgentEffects.UpdateStats();

        m_Health.UpdateMaximumHealth();
        m_AgentStamina.UpdateMaximumStamina();

        m_Health.Restore();
        m_AgentStamina.Restore();
    }

    private void LevelUp()
    {
        if (m_LevelUpCo != null)
        {
            StopCoroutine(m_LevelUpCo);
            m_LevelUpCo = null;
        }
        
        UpdateBaseStats();

        m_AgentEffects.UpdateStats();

        m_Health.UpdateMaximumHealth();
        m_AgentStamina.UpdateMaximumStamina();

        SoundManager.Instance.PlaySound(LevelUpSFX,transform.position);

        if (!IsDead)
        {
            m_Health.Restore();
            m_AgentStamina.Restore();
        }
    }
    
    public override void UpdateHealthUI()
    {
        m_CharacterIndicator.UpdateHealthBar();
    }

    public override void UpdateStaminaUI()
    {
        m_CharacterIndicator.UpdateStamina();
    }

    public void HideIndicator()
    {
        m_CharacterIndicator.HideIndicator();
    }

    protected override void UpdateBaseStats()
    {
        CharacterSO characterData = Data as CharacterSO;

        int MaxHealth = (int)(characterData.Stats.Health * (Mathf.Pow(characterData.StatsProgression.Health_Increment, LevelSystem.MAX_LEVEL / 5) / 100));
        int MaxAttack = (int)(characterData.Stats.Attack * (Mathf.Pow(characterData.StatsProgression.Attack_Increment, LevelSystem.MAX_LEVEL / 5) / 100));
        int MaxDefense = (int)(characterData.Stats.Defense * (Mathf.Pow(characterData.StatsProgression.Defense_Increment, LevelSystem.MAX_LEVEL / 5) / 100));
        int MaxStamina = (int)(characterData.Stats.Stamina * (Mathf.Pow(characterData.StatsProgression.Stamina_Increment, LevelSystem.MAX_LEVEL / 5) / 100));

        float LevelFormulaPart = (float)MatchManager.Instance.LevelSystem.Level / (float)LevelSystem.MAX_LEVEL;

        m_BaseStats.Health = Mathf.RoundToInt(characterData.Stats.Health + (MaxHealth - characterData.Stats.Health) * Mathf.Pow((LevelFormulaPart), characterData.StatsProgression.Health_Adjustment_Value));
        m_BaseStats.Attack = Mathf.RoundToInt(characterData.Stats.Attack + (MaxAttack - characterData.Stats.Attack) * Mathf.Pow((LevelFormulaPart), characterData.StatsProgression.Attack_Adjustment_Value));
        m_BaseStats.Defense = Mathf.RoundToInt(characterData.Stats.Defense + (MaxDefense - characterData.Stats.Defense) * Mathf.Pow((LevelFormulaPart), characterData.StatsProgression.Defense_Adjustment_Value));
        m_BaseStats.Stamina = Mathf.RoundToInt(characterData.Stats.Stamina + (MaxStamina - characterData.Stats.Stamina) * Mathf.Pow((LevelFormulaPart), characterData.StatsProgression.Stamina_Adjustment_Value));;
        
        m_BaseStats.WalkingSpeed = characterData.Stats.WalkingSpeed;
        m_BaseStats.RunningSpeed = characterData.Stats.RunningSpeed;
        m_BaseStats.CriticalChance = characterData.Stats.CriticalChance;
        m_BaseStats.CriticalDamage = characterData.Stats.CriticalDamage;
        
        CurrentStats = new Stats(m_BaseStats);
    }

    public void OnEvent(BlockCharacter eventType)
    {
        switch (eventType.Block)
        {
            case BlockCharacter.Type.Block:
                m_Movement.MovementSpeed = 0f;
                m_Animator.Play("Locomotion");
                break;
        }
    }
    
    public void OnEvent(TransitionEvent eventType)
    {
        switch (eventType.TransitionType)
        {
            case TransitionEvent.Type.Stop:
                m_Movement.MovementSpeed = 0f;
                m_Animator.Play("Locomotion");
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Killable"))
        {
            StartCoroutine(OnDeath(other.gameObject.transform));
        }
    }
}
