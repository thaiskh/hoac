﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AgentEffects : MonoBehaviour, EventListener<StateChangeEvent<AgentStates.AgentConditions>>
{
    public float RefreshRate = 1f;

    private Agent m_Agent;

    private List<Effect> m_Effects;
    private Coroutine m_Ticker;

    public List<Effect> CurrentBuffs
    {
        get { return m_Effects.Where(x => x is Buff).ToList(); }
    }
    
    public List<Effect> CurrentDebuffs
    {
        get { return m_Effects.Where(x => x is Debuff).ToList(); }
    }
    
    public List<Effect> CurrentEffects => m_Effects;

    private void Awake()
    {
        m_Agent = GetComponent<Agent>();
        m_Effects = new List<Effect>();
    }

    private void OnEnable()
    {
        this.EventStartListening<StateChangeEvent<AgentStates.AgentConditions>>();

        m_Ticker = StartCoroutine(TickCo());
    }

    private void OnDisable()
    {
        this.EventStopListening<StateChangeEvent<AgentStates.AgentConditions>>();
        
        StopCoroutine(m_Ticker);
    }

    private IEnumerator TickCo()
    {
        while (true)
        {
            Tick();
            yield return new WaitForSeconds(RefreshRate);
        }
    }
    
    private void Tick()
    {
        if (GameManager.Instance.Paused)
        {
            return;
        }

        foreach (var effect in m_Effects)
        {
            effect.Tick();
        }

        m_Effects.RemoveAll(x => x.Expired);
    }

    public void AddEffect(Effect effect)
    {
        Effect ef = m_Effects.Find(x => x.GetType() == effect.GetType());

        if (ef == null)
        {
            m_Effects.Add(effect);
            effect.Apply(m_Agent);
        }
        else
        {
            if (effect.Stackable)
            {
                m_Effects.Add(effect);
                effect.Apply(m_Agent);
            }
            else
            {
                ef.Refresh();
            }
        }
    }

    public void UpdateStats()
    {
        m_Effects.Where(x => x is StatBuff || x is StatDebuff).ToList().ForEach(x => x.Apply(m_Agent));
    }

    public void RemoveEffect(Effect effect)
    {
        m_Effects.Remove(effect);
    }
    
    public void CleanseDebuffs()
    {
        m_Effects.Where(x => x is Debuff).ToList().ForEach(x => x.Remove());
    }
    
    public void CleanseBuffs()
    {
        m_Effects.Where(x => x is Buff).ToList().ForEach(x => x.Remove());
    }
    
    public void CleanseAll()
    {
        m_Effects.ForEach(x => x.Remove());
    }

    public void OnEvent(StateChangeEvent<AgentStates.AgentConditions> eventType)
    {
        if (eventType.Target != gameObject
            || eventType.TargetStateMachine != m_Agent.ConditionState)
        {
            return;
        }

        if (eventType.NewState == AgentStates.AgentConditions.Dead)
        {
            CleanseAll();
        }
    }
}
