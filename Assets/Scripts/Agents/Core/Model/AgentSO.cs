﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AgentSO : ScriptableObject, IDatabaseAsset
{
    public int ID;

    public string NameKey;

    public Stats Stats;
    
    public int Id { get { return ID; } set { ID = value; } }
    public string Name { get { return NameKey; } set { NameKey = value; } }
}

[System.Serializable]
public struct Stats
{
    public int Health;
    public int Stamina;
    public float Attack;
    public float Defense;
    public int WalkingSpeed;
    public int RunningSpeed;
    [Range(0.0f, 1.0f)]
    public float CriticalChance;
    public float CriticalDamage;

    public Stats(Stats baseStats)
    {
        Health = baseStats.Health;
        Stamina = baseStats.Stamina;
        Attack = baseStats.Attack;
        Defense = baseStats.Defense;
        WalkingSpeed = baseStats.WalkingSpeed;
        RunningSpeed = baseStats.RunningSpeed;
        
        CriticalChance = baseStats.CriticalChance;
        CriticalDamage = baseStats.CriticalDamage;
    }
}
