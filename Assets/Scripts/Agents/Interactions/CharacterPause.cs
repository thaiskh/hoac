﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class CharacterPause : MonoBehaviour
{
    private PlayerInput m_PlayerInput;
    private Character m_Character;

    private void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Character = GetComponent<Character>();
    }

    private void Update()
    {
        if(m_Character.IsDead) return;

        if (GUIManager.Instance.PauseCanvas.gameObject.activeInHierarchy)
        {
            if (m_PlayerInput.Pause.GetButtonDown() || m_PlayerInput.Back.GetButtonDown())
            {
                GUIManager.Instance.ClosePauseMenu();
            }
        }
        else
        {
            if (GameManager.Instance.Paused || Time.deltaTime == 0f)
            {
                return;
            }

            if (m_PlayerInput.Pause.GetButtonDown())
            {
                GUIManager.Instance.OpenPauseMenu(transform);
            }
        }
    }
}
