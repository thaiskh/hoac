﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDiary : MonoBehaviour
{
    private PlayerInput m_PlayerInput;
    private Character m_Character;

    private void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Character = GetComponent<Character>();
    }

    private void Update()
    {
        if(m_Character.IsDead) return;

        if (GUIManager.Instance.DiaryCanvas.gameObject.activeInHierarchy)
        {
            if (m_PlayerInput.Diary.GetButtonDown() || m_PlayerInput.Back.GetButtonDown())
            {
                GUIManager.Instance.CloseDiary();
            }
        }
        else
        {
            if (GameManager.Instance.Paused || Time.deltaTime == 0f)
            {
                return;
            }

            if (m_PlayerInput.Diary.GetButtonDown())
            {
                GUIManager.Instance.OpenDiary(transform);
                EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Diary.ActionButton));
            }
        }
    }
}
