﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Defense Technique", menuName = "G1/Agent/Combat/Defense Technique")]
public class DefenseTechnique : CombatTechnique
{
    public override void TechniqueStart(Agent owner)
    {
        base.TechniqueStart(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.DefenseboxBehaviour.SetTechnique(this);
    }

    public override void PerformTechnique(Agent owner)
    {
        base.PerformTechnique(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.DefenseboxBehaviour.EnableBox(true);
    }

    public override void FinishTechnique(Agent owner)
    {
        base.FinishTechnique(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.DefenseboxBehaviour.EnableBox(false);
    }
    
    public override void TechniqueEnd(Agent owner)
    {
        base.TechniqueEnd(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.DefenseboxBehaviour.EnableBox(false);
    }
    
    public virtual void HitboxHit(Collider hitbox, HitTechnique technique)
    {
        if (!technique.DefenseAllowed)
        {
            return;
        }

        HitboxBehaviour hitboxBehaviour = hitbox.GetComponentInParent<HitboxBehaviour>();
        
        hitboxBehaviour.EnableBox(false);
    }
}
