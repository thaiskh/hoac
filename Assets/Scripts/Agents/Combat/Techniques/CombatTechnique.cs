﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CombatTechnique", menuName = "G1/Agent/Combat/Combat Technique")]
public class CombatTechnique : ScriptableObject
{
    public enum Type
    {
        Attack, Block, Dodge
    }

    [Header("Type")]
    public Type TechniqueType;

    public virtual void TechniqueStart(Agent owner)
    {
        
    }

    public virtual void TechniqueEnd(Agent owner)
    {
        
    }

    public virtual void PerformTechnique(Agent owner)
    {
        
    }

    public virtual void FinishTechnique(Agent owner)
    {
        
    }
}
