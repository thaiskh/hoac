﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Melee Technique", menuName = "G1/Agent/Combat/Melee Technique")]
public class MeleeTechnique : HitTechnique
{
    public override void PerformTechnique(Agent owner)
    {
        base.PerformTechnique(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.MeleeHitboxBehaviour.EnableBox(true);
    }

    public override void FinishTechnique(Agent owner)
    {
        base.FinishTechnique(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.MeleeHitboxBehaviour.EnableBox(false);
    }

    public override void TechniqueEnd(Agent owner)
    {
        base.TechniqueEnd(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();
        cSystem.MeleeHitboxBehaviour.EnableBox(false);
    }

    public override void TechniqueStart(Agent owner)
    {
        base.TechniqueStart(owner);

        AgentCombatSystem cSystem = owner.GetComponent<AgentCombatSystem>();

        cSystem.MeleeHitboxBehaviour.SetTechnique(this);
        cSystem.MeleeHitboxBehaviour.SetAgent(owner);
    }
}
