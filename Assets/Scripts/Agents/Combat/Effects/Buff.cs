﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Buff : Effect
{
    public Buff(float duration, bool stackable) : base(duration, stackable)
    {
        
    }
}

public abstract class StatBuff : Buff
{
    public StatBuff(float duration, bool stackable) : base(duration, stackable)
    {

    }
}

public class AttackBuff : StatBuff
{
    public float Quantity;

    public AttackBuff(float duration, bool stackable, float quantity) : base(duration, stackable)
    {
        Quantity = quantity;
    }

    public override void Apply(Agent agent)
    {
        base.Apply(agent);

        m_Agent.CurrentStats.Attack += Quantity;
    }

    public override void Remove()
    {
        base.Remove();

        m_Agent.CurrentStats.Attack -= Quantity;
    }
}

public class CriticalChanceBuff : StatBuff
{
    public float Quantity;

    public CriticalChanceBuff(float duration, bool stackable, float quantity) : base(duration, stackable)
    {
        Quantity = quantity;
    }

    public override void Apply(Agent agent)
    {
        base.Apply(agent);

        m_Agent.CurrentStats.CriticalChance += Quantity;
    }

    public override void Remove()
    {
        base.Remove();

        m_Agent.CurrentStats.CriticalChance -= Quantity;
    }
}

public class DefenseBuff : StatBuff
{
    public float Quantity;

    public DefenseBuff(float duration, bool stackable, float quantity) : base(duration, stackable)
    {
        Quantity = quantity;
    }

    public override void Apply(Agent agent)
    {
        base.Apply(agent);

        m_Agent.CurrentStats.Defense += Quantity;
    }

    public override void Remove()
    {
        base.Remove();

        m_Agent.CurrentStats.Defense -= Quantity;
    }
}

public class SpeedBuff : StatBuff
{
    public int Quantity;
    private int m_OldSpeed;

    public SpeedBuff(float duration, bool stackable, int quantity) : base(duration, stackable)
    {
        Quantity = quantity;
    }

    public override void Apply(Agent agent)
    {
        base.Apply(agent);

        m_OldSpeed = m_Agent.CurrentStats.WalkingSpeed;
        m_Agent.CurrentStats.WalkingSpeed = Quantity;
    }

    public override void Remove()
    {
        base.Remove();
        
        m_Agent.CurrentStats.WalkingSpeed = m_OldSpeed;
    }
}
