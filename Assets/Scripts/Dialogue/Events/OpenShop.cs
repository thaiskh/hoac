﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenShop : EventDialogue
{
    public override void Trigger()
    {
        GUIManager.Instance.CloseDialogue();

        NPCShop npc = (graph as DialogueGraph).Trigger.GetComponent<NPCShop>();
        GUIManager.Instance.OpenMenuShopScreen(npc, (graph as DialogueGraph).Opener);
    }

    public override void Skip()
    {
        Trigger();
    }
}
