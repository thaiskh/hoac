﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenQuest : EventDialogue
{
   public override void Trigger()
   {
      GUIManager.Instance.CloseDialogue();

      NPCQuest npc = (graph as DialogueGraph).Trigger.GetComponent<NPCQuest>();
      GUIManager.Instance.OpenAskQuestScreen(npc, (graph as DialogueGraph).Opener);
   }

   public override void Skip()
   {
      Trigger();
   }
}
