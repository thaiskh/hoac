﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue Agent", menuName = "G1/Dialogue Engine/Dialogue Agent")]
public class DialogueAgentData : ScriptableObject
{
   public string Name;
   public Sprite Portrait;
}
