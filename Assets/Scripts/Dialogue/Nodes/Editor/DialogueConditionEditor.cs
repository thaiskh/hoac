﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;


[CustomNodeEditor(typeof(DialogueCondition))]
public class DialogueConditionEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        DialogueCondition node = target as DialogueCondition;
        NodeEditorGUILayout.PortField(target.GetInputPort("input"));
        EditorGUILayout.Space();
        NodeEditorGUILayout.PortField(target.GetOutputPort("pass"));
        NodeEditorGUILayout.PortField(target.GetOutputPort("fail"));

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 200;
    }
}

