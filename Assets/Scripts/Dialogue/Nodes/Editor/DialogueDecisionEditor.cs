﻿using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;

[CustomNodeEditor(typeof(DialogueDecision))]
public class DialogueDecisionEditor : NodeEditor
{
   void AddInstancePorts(Node node)
   {
       node.AddInstanceInput(typeof(string), fieldName: "myDynamicInput");
       node.AddInstanceOutput(typeof(int), fieldName: "myDynamicOutput");
   }

   void RemoveInstancePorts(Node node)
   {
       node.RemoveInstancePort("myDynamicInput");
       node.RemoveInstancePort("myDynamicOutput");
   }

   public override void OnBodyGUI()
   {
       serializedObject.Update();
       DialogueDecision node = target as DialogueDecision;

       GUILayout.BeginHorizontal();

       NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"), GUILayout.MinWidth(0));
       GUILayout.EndHorizontal();

       EditorGUILayout.HelpBox("If the times talked is ____ than the number on the right, checks from top to bot and stops when one returns true", MessageType.Info);

       if (node.Types != null)
       {
           for (int i = 0; i < node.Types.Count; i++)
           {
               GUILayout.BeginHorizontal();
               node.Types[i] = (DialogueDecision.Type) EditorGUILayout.EnumPopup(node.Types[i], GUILayout.Width(75));
               node.number[i] = EditorGUILayout.IntField(node.number[i], GUILayout.Width(20));
               NodeEditorGUILayout.PortField(node.GetOutputPort("Dialogue " + i), GUILayout.MinWidth(100));
               GUILayout.EndHorizontal();
           }
       }

       GUILayout.BeginHorizontal();

       if (GUILayout.Button("NEW"))
       {
           node.Types.Add(new DialogueDecision.Type());
           node.number.Add(0);
           node.AddInstanceOutput(typeof(DialogueBaseNode), Node.ConnectionType.Override, Node.TypeConstraint.None, "Dialogue " + (node.Types.Count - 1));
       }

       if (GUILayout.Button("REMOVE"))
       {
           if (node.Types.Count > 0)
           {
               node.Types.RemoveAt(node.Types.Count - 1);
               node.number.RemoveAt(node.number.Count - 1);
               node.RemoveInstancePort(node.GetOutputPort("Dialogue " + node.Types.Count));
           }
       }

       GUILayout.EndHorizontal();
       NodeEditorGUILayout.PortField(target.GetOutputPort("DefaultDialogue"), GUILayout.MinWidth(0));

       serializedObject.ApplyModifiedProperties();
   }

   public override int GetWidth()
   {
       return 250;
   }
}
