﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(CloseDialogue))]
public class CloseDialogueEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        EventDialogue node = target as EventDialogue;

        NodeEditorGUILayout.PortField(target.GetInputPort("input"), GUILayout.Width(100));

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 150;
    }
}
