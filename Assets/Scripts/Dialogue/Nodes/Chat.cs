﻿using System;
using System.Collections.Generic;
using XNode;

[NodeTint("#CCFFCC")]
public class Chat : DialogueBaseNode
{
   public enum SpeakerType {Left, Right}

   public SpeakerType Speaker;
   public bool Instant;

   public string Text;
   
   [Output(instancePortList = true)] public List<string> Answers = new List<string>();

   public override DialogueBaseNode GetNextNode(int index)
   {
      NodePort port = null;

      if (Answers.Count == 0)
      {
         port = GetOutputPort("output");
      }
      else
      {
         if(Answers.Count <= index) throw new Exception(index.ToString());
         port = GetOutputPort("Answers " + index);
      }
      
      if(port == null) throw new Exception(index.ToString());
      NodePort nextPort = port.GetConnection(0);

      return nextPort.node as DialogueBaseNode;
   }

   public override void Trigger()
   {
      DialogueManager.Instance.ShowChat(this);
   }
}
